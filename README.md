# Yakabikaj

> Yakabikaj is a text search utility for Arabic-scripted texts. The search algorithm was developed by by Alicia Gonzalez Martinez within the ERC-Project COBHUNI in Hamburg University in collaboration with Thomas Milo from the company DecoType.

<img src="./yakabikaj.png" align="middle">

Yakabikaj performs string-search in any text in Arabic script. It focsed on Arabic and Persian languages and it is aimed at Academic uses.

## Examples of usage

In code:

```sh
>>> from yakabikaj import search
>>> for pat, res in search('کُتُب كَتَب کُتِب', ['كُتب']):
...   for tok, ini, end in res:
...     print(pat, tok, ini, end)
... 
كُتب کُتُب 0 5
كُتب کُتِب 12 17

```

As a script:

```sh
>>> echo کُتُب | yakabikaj --pat كُتب
کُتُب 0 5
```

```sh
>>> yakabikaj --text <(echo کُتُب) --pat كُتب
کُتُب 0 5
```

```sh
>>> yakabikaj --text <(echo "كتب الكُتُب") --pat_file <(echo -e "كتب\nکُتب\nكُتِب") --json | jq .
[
  {
    "pattern": "كتب",
    "found": [
      {
        "tok": "كتب",
        "ini": 0,
        "end": 3
      },
      {
        "tok": "كُتُب",
        "ini": 6,
        "end": 11
      }
    ]
  },
  {
    "pattern": "کُتب",
    "found": [
      {
        "tok": "كُتُب",
        "ini": 6,
        "end": 11
      }
    ]
  },
  {
    "pattern": "كُتِب",
    "found": []
  }
]
```

This version requires Python 3.8 or later.

## Dependencies

* ujson
* more-itertools

## Installation

```sh
$ make
```

## License
 
Yakabikaj is a text search utility for Arabic-scripted texts licensed under GPLv3.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Authors

Alicia González Martínez (ERC-Project | Universität Hamburg) and Thomas Milo

### Contact

Alicia González Martínez , *aliciagm85+kabikaj at gmail dot com*
