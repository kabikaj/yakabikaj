###########################################################################################################
#
# INSTALLATION AND TESTING FOR DEVELOPMENT
#
# Yakabikaj Copyright (C) 2019  Alicia González Martínez (aliciagm85+kabikaj at gmail dot com), Thomas Milo
#
###########################################################################################################

SHELL := /bin/bash

.PHONY : clean help all execute_tests install install_man_page

all: clean execute_tests install install_man_page

clean:
	@echo "\n>> Cleaning resources..."
	python3 setup.py clean --all
	find . -name __pycache__ -prune -exec rm -rf {} +
	rm -rf MANIFEST build* dist*

install:
	@echo "\n>> Installing yakabikaj library..."
	python3 -m py_compile yakabikaj/data.py
	python3 setup.py install

install_man_page:
	@echo "\n>> Installing man page..."
	sudo cp man/yakabikaj.1 /usr/share/man/man1/yakabikaj.1
	sudo gzip -f /usr/share/man/man1/yakabikaj.1
	mandb

execute_tests:
	@echo "\n>> Executing battery of tests..."
	python3 test/test_search.py test/tests_search
	
help:
	@echo "    all"
	@echo "        Clean resources and install package"
	@echo "    install"
	@echo "        install yakabikaj library"
	@echo "    install_man_page"
	@echo "        install man page."
	@echo "    clean"
	@echo "        remove resources"
	@echo "    execute_tests"
	@echo "        execute battery of tests"
	@echo ""
	@echo "usage: make [help] [all] [install] [install_man_page] [clean] [apply_test]"
