#
#    setup.py
#
# This code is licensed under GPLv3
# Copyright (C) 2020 Alicia González Martínez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
###########################################################################

from distutils.core import setup

def readme():
    with open('README.md') as fp:
        return fp.read()

setup(
    name = "yakabikaj",
    packages = ["yakabikaj"],
    version = "1.0b",
    description = "processing utility for Arabic-scripted texts",
    long_description = readme(),
    author = "Alicia González Martínez",
    author_email = "aliciagm85+kabikaj@gmail.com",
    url = "https://gitlab.com/abjad/yakabikaj.git",
    download_url = "git@gitlab.com:abjad/yakabikaj.git",
    scripts=['bin/yakabikaj'],
    keywords = ["search", "normalise", "unicode", "arabic", "persian", "farsi", "urdu", "NLP", "digital humanities"],
    license = 'GPLv3',
    classifiers = [
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Linguistic",
        "Natural Language :: Arabic",
        "Natural Language :: Persian",
        "Natural Language :: Urdu"
        ]
)