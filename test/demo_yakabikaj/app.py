#!/usr/bin/env python3.6
#
#    app.py
#
# prototype demo for search algorithm
#
# dependencies:
#
#   * bottle
#     $ conda install -c conda-forge bottle
#     http://zetcode.com/python/bottle/
#
#   * paste
#     $ conda install paste
#
# usage:
#   $ python app.py
#
# debug:
#   $ cd ../.. ; make && cd test ; python tester.py tests && cd demo_yakabikaj ; python app.py
#
# This code is licensed under GPLv3
# Copyright (C) 2020 Alicia González Martínez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
##############################################################################################

import os
import re
import json
from bottle import hook, route, run, post, request, response, \
                   static_file, template, error, BaseRequest

from yakabikaj import search

BaseRequest.MEMFILE_MAX = 2048 * 2048


_allow_origin = '*'
_allow_methods = 'PUT, GET, POST, DELETE, OPTIONS'
_allow_headers = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'

@hook('after_request')
def enable_cors():
    '''Add headers to enable CORS'''

    response.headers['Access-Control-Allow-Origin'] = _allow_origin
    response.headers['Access-Control-Allow-Methods'] = _allow_methods
    response.headers['Access-Control-Allow-Headers'] = _allow_headers

def mysearch(text, pat, exact, rasm, no_diac_synonym, ignore_diac, no_generic_ha, no_generic_hamza):
    """ Execute yakabikaj search.

    Args:
        text (str): text to search in.
        pat (list): patterns to search.
        exact (bool): indicate if excat must be performed (overrrides all other search options).
        rasm (bool): indicate if archigraphemic search is enabled.
        no_diac_synonym (bool): do not perform diacritic synonymity.
        ignore_diac(bool): ignore all diacritics in pattern for search.
        no_geneic_ha (bool): di not make all has match.
        no_generic_hamza (bool): do not make all hamza shapes match.

    """
    step = 0

    if exact:
        result = [(pat, ((m.group(), *m.span()) for m in re.compile(pat).finditer(text)))]

    else:
        result = search(text, [pat], config_params={'rasm'                : rasm ,
                                                    'diac_synonym'        : not no_diac_synonym ,
                                                    'diac_expansion'      : True ,
                                                    'ignore_diac'         : ignore_diac ,
                                                    'cluster_enable'      : True ,
                                                    'spaces_wild'         : True ,
                                                    'universal_ha'        : not no_generic_ha ,
                                                    'extended_ezafe'      : True ,
                                                    'multishape_hamza'    : not no_generic_hamza ,
                                                    'regex'               : False ,
                                                    'exact'               : exact
                                                    }, preproc=False, debug=True
                        )

    for _, res in result:

        print('@DEBUG@', res) #DEBUG
    
        if not res:
            return template('public/result.tpl', q=pat, restext='No matches') #FIXME


        for m, i, e in sorted(set(res), key=lambda x: (x[1], x[2])):
            s = '<span style="background-color: #FFFF00">%s</span>' % \
                text[i+step : e+step]
            text = text[:i+step] + \
                s + \
                text[e+step:]
            step += len(s)-len(text[i+step : e+step])
    
    return template('public/result.tpl', q=pat, restext=text)

def search_v2(text, pat, exact):
    if excat:
        result = [(pat, ((m.group(), *m.span()) for m in re.compile(pat).finditer(text)))]

    else:
        result = search(text, [pat], rasm=True)

    if not result:
        return json.dumps([])

    return json.dumps({'text'    : text,
                       'patterns' : [{
                             'pat': p ,
                             'results' : [{
                                'found' : f ,
                                'ini' : i ,
                                'end' : e
                                }
                                ]
                            } for p, res in result for f, i, e in res
                        ]}, ensure_ascii=False)

@route('/')
def server_static(filepath="index.html"):
    return static_file(filepath, root='./public/')

@error(404)
def error404(error):
    return 'queria yo hablarle de dostoievski'

@post('/result')
def process():

    pattern = request.forms.decode().get('q')
    text = request.forms.decode().get('text')

    no_diac_synonym = True if request.forms.get('no_diac_synonym') else False
    ignore_diac = True if request.forms.get('ignore_diac') else False
    no_generic_ha = True if request.forms.get('no_universal_ha') else False
    no_generic_hamza = True if request.forms.get('no_multishape_hamza') else False
    exact = True if request.forms.get('exact') else False
    rasm = True if request.forms.get('rasm') else False

    return mysearch(text, pattern, exact, rasm, no_diac_synonym, ignore_diac, no_generic_ha, no_generic_hamza)

@post('/search')
def process():
    body = json.loads(request.body.read())
    response.content_type = 'application/json'
    return search_v2(body['text'], body['q'], exact=False)

@route('/search', method = 'OPTIONS')
def options_handler(path = None):
    return


if __name__ == '__main__':

    run(host='0.0.0.0', port=8080, server='paste', debug=True)
