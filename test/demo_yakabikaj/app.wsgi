#!/usr/bin/env python3.6
#
# prototype demo
#
# dependencies:
#
#   * bottle
#     $ conda install -c conda-forge bottle
#     http://zetcode.com/python/bottle/
#
#   * paste
#     $ conda install paste
#
# usage:
#   $ python app.py
#
# Yakabikaj Copyright (C) 2019 Alicia González Martínez (aliciagm85+kabikaj at gmail dot com) and Thomas Milo (tmilo at decotype dot com)
#
#########################################################################################################################################

import os
import re
import json
import bottle

from yakabikaj import search

bottle.BaseRequest.MEMFILE_MAX = 2048 * 2048


_allow_origin = '*'
_allow_methods = 'PUT, GET, POST, DELETE, OPTIONS'
_allow_headers = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'

@bottle.hook('after_request')
def enable_cors():
    '''Add headers to enable CORS'''

    bottle.response.headers['Access-Control-Allow-Origin'] = _allow_origin
    bottle.response.headers['Access-Control-Allow-Methods'] = _allow_methods
    bottle.response.headers['Access-Control-Allow-Headers'] = _allow_headers

def mysearch(text, pat, regex, exact, rasm, no_diac_synonym, no_diac_expansion, cluster_enable,
        spaces_wild, universal_ha, extended_ezafe, multishape_hamza):
    """ Execute yakabikaj search.

    Args:
        text (str): text to search in.
        pat (list): patterns to search.
        regex (bool): suport regex.
        exact (bool): indicate if excat must be performed (overrrides all other search options).
        rasm (bool): indicate if archigraphemic search is enabled.

        no_diac_synonym (bool): do not perform diacritic synonymity.
        no_diac_expansion (bool): do not perform diacritic expansion.
        cluster_enable (bool): enable cluster support.
        spaces_wild (bool): may spaces be optional after each letterblock.
        universal_ha (bool): make all ha's match.
        extended_ezafe (bool): support different orthographies and encoding variations for the Persian ezafe.
        multishape_hamza (bool): make all hamza shapes match.

    """
    step = 0

    if exact:
        result = [(pat, ((m.group(), *m.span()) for m in re.compile(pat).finditer(text)))]

    else:
        result = search(text, [pat], {'rasm'                : rasm ,
                                      'diac_synonym'        : not no_diac_synonym ,
                                      'diac_expansion'      : not no_diac_expansion ,
                                      'cluster_enable'      : cluster_enable ,
                                      'spaces_wild'         : spaces_wild ,
                                      'universal_ha'        : universal_ha ,
                                      'extended_ezafe'      : extended_ezafe ,
                                      'multishape_hamza'    : multishape_hamza ,
                                      'regex'               : regex ,
                                      'exact'               : exact
                                      }, debug=True #DEBUG
                        )


    for _, res in result:
    
        if not res:
            return bottle.template('public/result.tpl', q=pat, restext='No matches') #FIXME


        for m, i, e in sorted(set(res), key=lambda x: (x[1], x[2])):
            s = '<span style="background-color: #FFFF00">%s</span>' % \
                text[i+step : e+step]
            text = text[:i+step] + \
                s + \
                text[e+step:]
            step += len(s)-len(text[i+step : e+step])
    
    return bottle.template('public/result.tpl', q=pat, restext=text)

def search_v2(text, pat, exact):
    if excat:
        result = [(pat, ((m.group(), *m.span()) for m in re.compile(pat).finditer(text)))]

    else:
        result = search(text, [pat], rasm=True)

    if not result:
        return json.dumps([])

    return json.dumps({'text'    : text,
                       'patterns' : [{
                             'pat': p ,
                             'results' : [{
                                'found' : f ,
                                'ini' : i ,
                                'end' : e
                                }
                                ]
                            } for p, res in result for f, i, e in res
                        ]}, ensure_ascii=False)

@bottle.route('/')
def server_static(filepath="index.html"):
    return bottle.static_file(filepath, root='./public/')

@bottle.error(404)
def error404(error):
    return 'queria yo hablarle de dostoievski'

@bottle.post('/result')
def process():

    pattern = bottle.request.forms.decode().get('q')
    text = bottle.request.forms.decode().get('text')

    no_diac_synonym = True if bottle.request.forms.get('no_diac_synonym') else False
    no_diac_expansion = True if bottle.request.forms.get('no_diac_expansion') else False
    cluster_enable = True if bottle.request.forms.get('cluster_enable') else False
    spaces_wild = True if bottle.request.forms.get('spaces_wild') else False
    universal_ha = True if bottle.request.forms.get('universal_ha') else False
    extended_ezafe = True if bottle.request.forms.get('extended_ezafe') else False
    multishape_hamza = True if bottle.request.forms.get('multishape_hamza') else False

    regex = True if bottle.request.forms.get('regex') else False
    exact = True if bottle.request.forms.get('exact') else False
    rasm = True if bottle.request.forms.get('rasm') else False

    return mysearch(text, pattern, regex, exact, rasm, no_diac_synonym, no_diac_expansion, cluster_enable,
                    spaces_wild, universal_ha, extended_ezafe, multishape_hamza)

@bottle.post('/search')
def process():
    body = json.loads(bottle.request.body.read())
    bottle.response.content_type = 'application/json'
    return search_v2(body['text'], body['q'], exact=False)

@bottle.route('/search', method = 'OPTIONS')
def options_handler(path = None):
    return


if __name__ == '__main__':

    bottle.run(host='0.0.0.0', port=8080, server='paste', debug=True)
