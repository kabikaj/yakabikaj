#!/usr/bin/env python3.6
#
# prototype demo
#
# dependencies:
#
#   * bottle
#     $ conda install -c conda-forge bottle
#     http://zetcode.com/python/bottle/
#
#   * paste
#     $ conda install paste
#
# usage:
#   $ python app.py
#
# Yakabikaj Copyright (C) 2019 Alicia González Martínez (aliciagm85+kabikaj at gmail dot com) and Thomas Milo (tmilo at decotype dot com)
#
#########################################################################################################################################

import os
import re
import sys
import json
from bottle import route, run, post, request, response, \
                   static_file, template, error, BaseRequest

from yakabikaj import search

BaseRequest.MEMFILE_MAX = 2048 * 2048



def mysearch(text, pat, std):
    """ Execute yakabikaj search.

    Args:
        text (str): text to search in.
        pat (list): patterns to search.
        std (bool): indicate if standard search must be performed.

    """
    step = 0

    if std:
        result = [(pat, ((m.group(), *m.span()) for m in re.compile(pat).finditer(text)))]

    else:
        result = search(text, [pat], rasm=True)
        
    #for _, res in result:
    #
    #    if not res:
    #        return template('public/result.tpl', q=pat, restext='No matches') #FIXME
    #    
    #    for m, i, e in res:
    #        s = '<span style="background-color: #FFFF00">%s</span>' % \
    #            text[i+step : e+step]
    #        text = text[:i+step] + \
    #            s + \
    #            text[e+step:]
    #        step += len(s)-len(text[i+step : e+step])
    #return template('public/result.tpl', q=pat, restext=text)

    if not result:
        return json.dumps([])
    
    return json.dumps({'text'    : text,
                       'patterns' : [{
                             'pat': p ,
                             'results' : [{
                                'found' : f ,
                                'ini' : i ,
                                'end' : e
                                }
                                ]
                            } for p, res in result for f, i, e in res
                        ]}, ensure_ascii=False)



@route('/')
def server_static(filepath="index.html"):
    return static_file(filepath, root='./public/')

@error(404)
def error404(error):
    return 'queria yo hablarle de dostoievski'

#@post('/result')
#def process():
#    pattern = request.forms.decode().get('q')
#    text = request.forms.decode().get('text')
#    std = True if request.forms.get('std_search') else False

@post('/result')
def process():
    body = json.loads(request.body.read())
    return mysearch(body['text'], body['q'], std=False) #FIXME  it's sentind html instead of json


if __name__ == '__main__':

    response.set_header('Access-Control-Allow-Origin', '*')
    response.add_header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS')

    run(host='0.0.0.0', port=8080, server='paste', debug=True)
