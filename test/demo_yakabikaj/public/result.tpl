<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Yakabikaj Demo</title>
</head>

<body bgcolor="#e0edd3" style="font-family: Amiri; line-height: 2.4;">

        <div>
            <h2 style="color:#31493c; font-size: 38px">Arabic Unicode Search!</h2>
        </div>

        <div style="text-align:right; font-size:48px;">{{q}}</div>

        <div contenteditable="true" style="text-align:right; font-size:48px;">
            <p/>
            {{!restext}}
        </div>

        <input type="button" value="Back" style="position: absolute; right: 0; height:40px; width:150px" onclick="window.history.back()" />

</body>

</html>
