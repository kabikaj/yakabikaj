#!/usr/bin/env python3
#
#    tester.py
#                                              
# tester for search.py                               ا
#                                                   ح   ه
#                                                  ف   ذ   ا   
#                                                 ظ   ا   ل   ي   
#                                                ز       م   ا   ك   
#                                                       ص       ب
#                                                      و       ي
#                                                     د       ك
#                                                    ر       ج             
#                                                    
# examples:
#   $ python test_search.py tests_search --preproc
#   $ cd .. ; make &>/dev/null && cd - ; python tester_search.py tests_search --range 4,54 --debug &> $(date +%s).log
#
# profiling:
#   $ hyperfine 'yakabikaj input_text --patterns_file input_patterns' --warmup 1
#
#
# This code is licensed under GPLv3
# Copyright (C) 2020 Alicia González Martínez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
####################################################################################################################

import sys
from argparse import ArgumentParser, FileType, ArgumentTypeError
from yakabikaj import search

RED   = '\033[91m'
RESET = '\033[00m'


def parse_range(arg):
    """ Check if arg is a pair of n,m integers indicating a range.

    Args:
        arg (str): range of lines.

    Return:
        tuple: n, m  range of lines.

    Raise:
        ArgumentTypeError: is the format of arg is not n,m.

    """
    try:
        n, m = arg.split(',')
        return int(n.strip()), int(m.strip())
    except:
        raise ArgumentTypeError('argument format must be n,m, eg. 10,15 ')


if __name__ == '__main__':

    parser = ArgumentParser(description='tester for search algorithm')
    parser.add_argument('infile', nargs='?', type=FileType('r'), default=sys.stdin, help='tests')
    parser.add_argument('outfile', nargs='?', type=FileType('w'), default=sys.stdout, help='result')
    parser.add_argument('--line', type=int, help='pass only test of specified line from file')
    parser.add_argument('--range', type=parse_range, help='pass tests from range of lines (n,m)')
    parser.add_argument('--preproc', action='store_true', help='pass tests with preproc set on')
    parser.add_argument('--debug', action='store_true', help='debug mode')
    args = parser.parse_args()

    tests = ((i,*li.split()) for i,li in enumerate((l.partition('#')[0].strip() for l in args.infile), 1) if li)

    correct, wrong = 0, 0
    for n, text, pattern, expected, *config_info in tests:

        # indexes must be preceded by % and follow by space, e.g. % 2:15
        if len(config_info) == 14:
            indexes = config_info[-1].split(':')
            config_info = config_info[:-2]
            
        else:
            indexes = None

        if args.range and not (args.range[0] <= n and args.range[1] >=  n):
            continue

        if args.line and args.line != n:
            continue

        config = dict(zip(('rasm', 'diac_synonym', 'diac_expansion', 'ignore_diac', 'cluster_enable', 'spaces_wild',
                           'multishape_hamza', 'universal_ha', 'extended_ezafe', 'ignore_alif', 'regex', 'exact'),
                 ((True if c=='T' else False) for c in config_info)))

        text = text.replace('_', ' ')
        pattern = pattern.replace('_', ' ')

        got = list(res for pat, res in search(text, [pattern], config_params=config, preproc=args.preproc, debug=args.debug))[0]

        if expected == 'FAIL':

            if not got:
                print('OK     (%d) text="%s" pattern="%s" expected="%s"' % (n, text, pattern, expected), file=args.outfile)
                correct += 1

            else:
                print('%sERROR! (%d) match NOT expected, text="%s" pattern="%s" expected="%s"%s' % (RED, n, text, pattern, expected, RESET), file=args.outfile)
                wrong += 1

        elif expected == 'OK':

            if not got:
                print('%sERROR! (%d) match expected, text="%s" pattern="%s" expected="%s"%s' % (RED, n, text, pattern, expected, RESET), file=args.outfile)
                wrong += 1

            else:

                if indexes:

                    if got[0][1] == int(indexes[0]) and got[0][2] == int(indexes[1]):

                        print('OK     (%d) text="%s" pattern="%s" expected="%s" ' % (n, text, pattern, expected), end='', file=args.outfile)
                        print('; '.join('(%d:%d)' % (ini, end) for _, ini, end in got), file=args.outfile)
                        correct += 1

                    else:
                        print('%sERROR! (%d) match occurred but indexes don\'t match, text="%s" pattern="%s" expected="%s"%s got_indexes="%d:%d" expected_indexes="%s:%s"' %
                            (RED, n, text, pattern, expected, RESET, got[0][1], got[0][2], indexes[0], indexes[1]), file=args.outfile)
                        wrong += 1

                else:            
                    print('OK     (%d) text="%s" pattern="%s" expected="%s" ' % (n, text, pattern, expected), end='', file=args.outfile)
                    print('; '.join('(%d:%d)' % (ini, end) for _, ini, end in got), file=args.outfile)
                    correct += 1

        else:

            print('%sFATAL ERROR in line %d parsing expected result%s' % (RED, n, RESET), file=sys.stderr)
            sys.exit(1)

    print('\nNo. correct   = %d' % correct)
    print('No. incorrect = %d' % wrong)

