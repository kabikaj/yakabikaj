#!/usr/bin/env python3
#
#    query.py
#
# build and handle form to initiate search queries
#
#                        |            |     _)  |           _)
#      |   |   _` |      |  /   _` |  __ \   |  |  /   _` |  |
#      |   |  (   |        <   (   |  |   |  |    <   (   |  |
#      \__,| \__,_|     _|\_\ \__,_| _.__/  _| _|\_\ \__,_|  |
#      ____/                                              ___/
#
#
# This code is licensed under GPLv3
# Copyright (C) 2021 COBHUNI - ERC-Project | Universität Hamburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
##########################################################################################################


from django import forms
from yakabikaj import search
from yakabikaj_gui.result import Result


class QueryForm(forms.Form):
    """ Build Django query form """

    # TEXT to search
    TEXT = forms.CharField(widget=forms.Textarea(attrs={"rows": 5, "cols": 20, "class": "dir-ltr"}))

    # [PAT ...] pattern to search
    PAT = forms.CharField(max_length=200)

    # perform preprocessing to speed out the search (works only when pattern contains complete letterblocks)
    PREPROC = forms.BooleanField(required=False, initial=False)

    # block synonym diacritics to match
    DIAC_SYNONYM = forms.BooleanField(required=False, initial=False)

    # perform archigraphemic search (automatically set spaces_wild to True)
    RASM = forms.BooleanField(required=False, initial=False)

    # ignore all diacritics that appear in pattern
    IGNORE_DIAC = forms.BooleanField(required=False, initial=False)

    # make all letter ha match
    UNIVERSAL_HA = forms.BooleanField(required=False, initial=True)

    # do not match all hamzas among themselves
    MULTISHAPE_HAMZA = forms.BooleanField(required=False, initial=False)

    # perform exact search (irgnore rest of configuration)
    EXACT = forms.BooleanField(required=False, initial=False)

    # switch RTL and LTR font direction
    RTL = forms.BooleanField(required=False, initial=True)

    def __str__(self):
        return self.PAT


class Query:
    """ Handles search queries initiated in GUI

    Args:
        data (list): cleaned data of submitted form.
    """

    data = []

    def __init__(self, data):
        self.data = data

    def dispatch(self):
        """ Dispatch and process search query

        Yield:
            result (Result): processed search result containing fulltext, matches and markups.
        """

        text = self.data['TEXT']
        patterns = self.data['PAT']
        preproc = self.data['PREPROC']
        config_params = self.get_config(self.data)

        search_result = search(text, [patterns], None, config_params, preproc)

        result = Result(text, search_result)
        result.parse_search()

        return result

    def get_config(self, data):
        """ Get nginx from submitted form data

        Yield:
            nginx (dict): nginx containing all parsed form values.
        """

        return {
            'rasm': data.get('RASM', False),
            'diac_synonym': data.get('DIAC_SYNONYM', True),
            'diac_expansion': True,
            'ignore_diac': data.get('IGNORE_DIAC', False),
            'ignore_alif': False,
            'cluster_enable': True,
            'spaces_wild': True,
            'universal_ha': data.get('UNIVERSAL_HA', True),
            'extended_ezafe': True,
            'multishape_hamza': data.get('MULTISHAPE_HAMZA', False),
            'regex': False,
            'exact': data.get('EXACT', False),
        }
