#!/usr/bin/env python3
#
#    result.py
#
# search result
#
#                        |            |     _)  |           _)
#      |   |   _` |      |  /   _` |  __ \   |  |  /   _` |  |
#      |   |  (   |        <   (   |  |   |  |    <   (   |  |
#      \__,| \__,_|     _|\_\ \__,_| _.__/  _| _|\_\ \__,_|  |
#      ____/                                              ___/
#
#
# This code is licensed under GPLv3
# Copyright (C) 2021 COBHUNI - ERC-Project | Universität Hamburg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
##########################################################################################################


from yakabikaj_gui.settings import MIN_CONTEXT_OFFSET, MARK_MORE_CONTEXT, HIGHLIGHT_PATTERN


class Result:
    """ Handles search queries initiated in GUI

    Args:
        source (str): submitted search text.
        search (str, list): processed search result as pattern and sequence of tuples
                            containing matched string, ini offset, end offset.
    """

    source = ''
    search = []
    matches = []
    fulltext = ''

    def __init__(self, source, search):
        self.source = source
        self.search = search


    def parse_search(self):
        """ Parse search result data

        Yield:
            str: fulltext as HTML containing markups for match highlighting
            list: parsed matches
        """

        parsed_matches = []
        idx = 1

        for m in self.search:
            for r in m[1]:
                parsed_matches.append({
                    'idx': idx,
                    'pattern': m[0],
                    'search': r[0],
                    'offset': {
                        'from': r[1],
                        'to': r[2],
                        'range': '{} - {}'.format(r[1], r[2])
                    },
                    'context': self.put_into_context(r[1], r[2])
                })
                idx += 1

        self.fulltext = self.put_into_fulltext(parsed_matches)
        self.matches = parsed_matches

        return self.fulltext, self.matches


    def put_into_context(self, of, ot):
        """ Build context of match with offset from 'of' to 'ot'

        Args:
            of (int): ini offset
            ot (int): end offset

        Yield:
            dict: dictionary containing context of match and html markup for highlighting
        """

        s = self.source
        o = MIN_CONTEXT_OFFSET

        ofm = (of > o) and s.rfind(' ', 0, of - o) or 0
        ofp, mark_f = (ofm > 0) and (ofm, MARK_MORE_CONTEXT) or (0, '')

        otm = (ot + o < len(s)) and s.find(' ', ot + o) or len(s)
        otp = (otm > 0) and otm or len(s)
        mark_t = (otp < len(s)) and MARK_MORE_CONTEXT or ''

        ct = {
            'before': mark_f + s[ofp:of],
            'match': s[of:ot],
            'after': s[ot:otp] + mark_t
        }

        ct['html'] = format_html_highlight(ct, of, ot)

        return ct


    def put_into_fulltext(self, matches):
        """ Build full-text with HTML markups for highlighting from source

        Args:
            matches (list): parsed matches containing match, pattern, offset and context

        Yield:
            str: full-text with HTML markups
        """

        s = self.source
        pointer = 0
        fulltext = ''

        for m in matches:
            o = m['offset']
            fulltext += format_html_highlight({
                'before': s[pointer:o['from']],
                'match': s[o['from']:o['to']],
                'after': ''
            }, o['from'], o['to'])
            pointer = o['to']

        fulltext += s[pointer:len(s)]

        return fulltext


def format_html_highlight(ct, of, ot):
    """ Returns string with HTML markups for highlighting

    Args:
        ct (dict): context of match (before, after, match)
        of (int): ini offset
        ot (int): end offset

    Yield:
        str: string of context with HTML markups
    """

    return HIGHLIGHT_PATTERN.format(
        ct['before'],
        'highlight match-{}-{}'.format(of, ot),
        ct['match'],
        ct['after']
    )
