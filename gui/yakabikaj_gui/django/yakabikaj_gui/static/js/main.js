(function( $ ) {


    /**
     * hold GUI elements and values
     */
    var yGUI = {
        settingsBox: $('#query-settings'),
        formBox: $('#query-form'),
        settingsToggle: $('#toggle-query-settings'),
        settingsCode: $('#manage-settings .current-settings-code'),
        formToggle: $('#toggle-query-form'),
        switchRtlLtr: $('#ltr-rtl-switch input'),
        inputText: $('#id_TEXT'),
        inputPattern: $('#id_PAT'),
        resultList: $('#result-list'),
        resultFulltext: $('#result-fulltext'),
        clicked: false,

        /**
         * toggle the settings box
         */
        toggleSettings: function() {
            if (yGUI.settingsBox.height() > 0) {
                yGUI.settingsToggle.html("Show settings");
                yGUI.settingsBox.css("max-height", "0px");
            } else {
                yGUI.settingsToggle.html("Hide settings")
                yGUI.settingsBox.css("max-height", document.body.clientHeight + "px");
            }
        },


        /**
         * toggle the form box
         */
        toggleForm: function() {
            [yGUI.formBox, yGUI.formToggle].map(function(el) {
                el.toggleClass('d-none');
            });
        },


        /**
         * set font direction of input form and result output
        * @param {string} dir           Direction as string: 'dir-rtl' (RTL) or 'dir-ltr' (LTR).
         */
        setDirection: function(dir) {
            [yGUI.inputText, yGUI.inputPattern, yGUI.resultList, yGUI.resultFulltext].map(function(el) {
                if (typeof el === 'undefined' || el === null) return;

                el.removeClass('dir-ltr', 'dir-rtl');
                el.addClass(dir);
            });

            yGUI.switchRtlLtr.prop('checked', dir == 'dir-rtl');
            localStorage.setItem('direction', dir);
        },


        /**
         * mark a (already highlighted) match
        * @param {element} el           DOM element which is marked
         */
        markMatch: function(el) {
            $('span.highlight, .result-list-item').removeClass('mark');
            $(el).addClass('mark');
        },


        /**
         * ensure that the element is in center of the viewport by scrolling the window
        * @param {element} el           DOM element that should be in the center of viewport
         */
        centerElementInViewport: function(el) {
            $([document.body, document.documentElement]).animate({
                scrollTop: $(el).offset().top - ($(window).height() / 2)
            }, 200, function() {
                setTimeout(function() { yGUI.clicked = false; }, 1000);
            });
        },


        /**
         * build Bootstrap tooltips
         */
        buildTooltips: function() {
            var ts = $('[data-bs-toggle="tooltip"]');
            [].slice.call(ts).map(function (t) {
                return new bootstrap.Tooltip(t)
            })
        },


        /**
         * build Bootstrap modal dialogs
         */
        buildModals: function(modals) {
            [].slice.call(modals).map(function (m) {
                $(m).modal({ keyboard: false });
            })
        },

        /**
         * load configuration by settings code
         */
        loadSettings(code) {
            var config = (code >>> 0).toString(2).split('').reverse();
            yGUI.settingsBox.find('div.query-setting input:checked').prop('checked', false);

            $.each(config, function( idx, val ) {
                if (val > 0) {
                    var label = yGUI.settingsBox.find('div.query-setting label[data-id="' + (idx + 1) + '"]');
                    $(label).parent('div.query-setting').find('input').prop('checked', true);
                }
            });

            yGUI.updateSettingsCode();
        },


        /**
         * generate code of current configuration
         */
        generateSettingsCode() {
            var settings = yGUI.settingsBox.find('div.query-setting input:checked');
            var buffer = new Array(50).fill('0');

            [].slice.call(settings).map(function (s) {
                buffer[parseInt($(s).next('label').data('id')) - 1] = '1' ;
            });

            return parseInt(buffer.reverse().join(''), 2);
        },


        /**
         * init handler to mark matches
        * @param {array} handler           array of DOM elements that initiate and intercept a marker
         */
        initMarkHandler: function(handler) {
            $(handler).map(function(i, el) {
                var o_from = $(el).data('offset-from'),
                    o_to = $(el).data('offset-to'),
                    ftEl = $('.match-' + o_from + '-' + o_to);

                $(el).off('mouseenter').on('mouseenter', function(e) {
                    if (!yGUI.clicked) yGUI.markMatch(ftEl);
                });

                $(el).off('click').on('click', function(e) {
                    yGUI.clicked = true
                    yGUI.markMatch(ftEl);
                    yGUI.centerElementInViewport(ftEl);
                });
            });
        },


        /**
         * init handler to show modal dialogs
        * @param {array} handler           array of DOM elements that initiate and intercept a modal dialog
         */
        initModalHandler: function(handler) {
            $(handler).map(function(i, el) {
                $(el).off('click').on('click', function(e) {
                    $($(el).data('target')).modal('show');
                });
            });
        },


        /**
         * init handler to load configuration set by settings code
        * @param {array} handler           array of DOM elements that initiate and intercept a modal dialog
         */
        initLoadHandler: function(handler) {
            $(handler).map(function(i, el) {
                $(el).off('hide.bs.modal').on('hide.bs.modal', function(e) {
                    if (document.activeElement.id == 'btnLoadConfig') {
                        yGUI.loadSettings($(el).find('input#load_settings_code').val());
                    }
                });
            });
        },


        /**
         * init handler to handle changes of configuration set
        * @param {array} handler           array of DOM elements that initiate and intercept a modal dialog
         */
        initChangeHandler: function(handler) {
            $(handler).map(function(i, el) {
                $(el).off('change.gui').on('change.gui', function(e) {
                    yGUI.updateSettingsCode();
                });
            });
        },




        /**
         * calculate current configuration code and update frond-end
        * @param {array} handler           array of DOM elements that initiate and intercept a modal dialog
         */
        updateSettingsCode: function() {
            yGUI.settingsCode.html(yGUI.generateSettingsCode());
        },


        /**
         * initialize the Graphical User Interface
         */
        init: function() {
            // initialize click event handler
            yGUI.settingsToggle.off('click').on('click', yGUI.toggleSettings);
            yGUI.formToggle.off('click').on('click', yGUI.toggleForm);
            yGUI.switchRtlLtr.off('click').on('click', function() {
                yGUI.setDirection(yGUI.switchRtlLtr.prop('checked') ? 'dir-rtl' : 'dir-ltr');
            });

            yGUI.initMarkHandler($('.result-list-item'));
            yGUI.buildTooltips();

            // init font direction
            var dir = localStorage.getItem('direction');
            yGUI.setDirection( typeof dir === 'undefined' || dir === null ? 'dir-rtl' : dir);

            // init modals
            var modals = $('[data-toggle="modal"]');
            yGUI.initModalHandler(modals);
            yGUI.buildModals(modals);

            // init configuration loader
            yGUI.initLoadHandler($('#loadConfig'));
            yGUI.initChangeHandler(yGUI.settingsBox.find('div.query-setting input'));
            yGUI.updateSettingsCode();
        },
    };


    /**
     * initialize when document.ready state reached
     */
    $(document).ready(function() {
        yGUI.init();
        console.log('GUI initiated');
    });

})( jQuery );