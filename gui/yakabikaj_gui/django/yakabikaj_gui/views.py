from django.shortcuts import render

from yakabikaj_gui.query import QueryForm, Query


def index(request):
    """ Render form and results of Yakabikaj search

    Args:
        request (HttpRequest): browser request
    """

    if request.method == 'POST':
        form = QueryForm(request.POST)

        if form.is_valid():
            # form is submitted and valid. let's process the data

            query = Query(form.cleaned_data)

            context = {
                'form': form,
                'result': query.dispatch(),
                'submitted': True
            }

    else:
        form = QueryForm()
        context = {'form': form, }

    return render(request, 'index.html', context)
