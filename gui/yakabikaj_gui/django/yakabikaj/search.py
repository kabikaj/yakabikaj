#!/usr/bin/env python3
#
#    search.py
#
# search algorithm for Arabic-scripted texts
#
#
#                        |            |     _)  |           _) 
#      |   |   _` |      |  /   _` |  __ \   |  |  /   _` |  | 
#      |   |  (   |        <   (   |  |   |  |    <   (   |  | 
#     \__, | \__,_|     _|\_\ \__,_| _.__/  _| _|\_\ \__,_|  |        احفظ هذا المصودر يا كبيكج يا كبيكج
#     ____/                                              ___/  
#
# dependencies:
#   * more_itertools
#
# TODO
# ----
#  * support for this one لِ لّ َ ذِينَ, الَّذِينَ ?
#  * support for date conversions
#  * match all hamzas among themselves!! not just isolated hamza
#  * add treatment of quranic punctuation
#  * add preprocessing using ngrams of letterblocks; --preprocc {on, off, auto}
#  * arreglar indices mal ajustados!! tests 160 y 161
#  *!! mira a ver si el flag cluster enable esta funcionando bien
#  * maybe keep args.text in search as a _io._TextIOBase ?? (this is the parent if io.StringIO and io.TextIOWrapper)
#  * log file instead of debug
#
# debbuging usage:
#   ~/Dropbox/yakabikaj/test $ cd .. ; python setup.py install >/dev/null && echo شيئ | yakabikaj search --pattern شيء
#   ~/Dropbox/yakabikaj/test $ cd .. ; make &>/dev/null && cd - ; python tester.py tests --range 4,62
#   ~/Dropbox/yakabikaj/test $ cd .. ; make &>/dev/null && cd - ; python tester.py tests --line 49 --debug
#
# This code is licensed under GPLv3
# Copyright (C) 2021 Alicia González Martínez and Thomas Milo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
##############################################################################################################################

import re
import sys
from itertools import groupby, islice, tee
from more_itertools import split_after

from yakabikaj.data import SearchConfig, TAG, Segment


def _build_action(tag):
    """ Create arg action for bulding the arg lexicon that must be passed to the scanner.

    Gaylads::it's warmer than the sunlight

    Args:
        tag (TAG): label for token.

    Return:
        function: closure function.

    """
    return lambda scanner, token: (tag, token)

def _split(pattern, rules):
    """ Split pattern into segments to expand them afterwards.

    Args:
        pattern (str): text to split.
        rules (list): pairs of tags, regex describing the segments.

    Return:
        list: pairs of tags, matched subtexts

    """    
    scanner = re.Scanner([
        (token, _build_action(tag)) for tag, token in rules
    ], flags=re.DOTALL)

    #print('><', rules, scanner.scan(pattern)) #DEBUG

    results, _ = scanner.scan(pattern)

    return results

def join_boundaries(segments, num_punct):
    """ Join boundary segments and extend numbers and punctuation.

    Args:
        segments (iter): sequence of segments.
        num_punct (dict): inventory of number and punctuation equivalences.

    Yield:
        tuple: updated segment.

    """
    titivillus = []

    for tag, tok in segments:

        if tag is TAG.BOUN:
            titivillus.append(num_punct.get(tok, (tok if tok == ' ' else re.escape(tok))))

        else:
            if titivillus:
                yield TAG.BOUN, ''.join(titivillus)
                titivillus = []

            yield tag, tok

    if titivillus:
        yield TAG.BOUN, ''.join(titivillus)

def _segment(pattern, config, debug=False):
    """ Tag and identify segments in pattern and expand vowels and expand boundaries.

    Args:
        pattern (str): pattern to segment.
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.

    Yield:
        Segment: extended and annotated segments from pattern.

    """
    segmented = list(join_boundaries(_split(pattern, config.rules), config.NUM_PUNCT))

    n = len(segmented)

    # calculate ini and end indexes of letterblocks
    letterblock_gr = [list(split_after(g, lambda y: y[1] == TAG.BASE_DIS)) for k, g in
                      groupby(((i, tag) for i, (tag, tok) in enumerate(segmented) if tag != TAG.DIAC), key=lambda x: x[1]==TAG.BOUN) if not k]

    ini_letterblocks = [i[0][0] for subli in letterblock_gr for i in subli]
    end_letterblocks = [i[-1][0] for subli in letterblock_gr for i in subli]

    i = 0
    while i < n:
        tag, tok = segmented[i]

        ## TAG.CLUS ##

        if tag == TAG.CLUS:
            if debug:
                print('@debug@ * cluster %s in pattern' % tok, file=sys.stderr) #TRACE

            yield from config.clusters_pat[tok]

        ## TAG.DIAC ##

        elif tag == TAG.DIAC:
            yield Segment(tag, tok)

        ## TAG.BOUN ##

        elif tag == TAG.BOUN:
            tok_exp = tok

            if re.match('^%s$' % SearchConfig.SPACE_OPT, tok):
                
                if config.spaces_wild:
                    tok_exp = '(?:%s)*?' % tok
            
            elif re.match('^%s$' % config.OPT_NOTEXT, tok):
                tok_exp = config.OPT_NOTEXT

            yield Segment(tag, tok, exp=tok_exp)

        ## TAG.BASE_(CON|DIS) ##

        else:
            yield Segment(tag, tok, i in ini_letterblocks, i in end_letterblocks)

            if i < n-1 and segmented[i+1][0] != TAG.DIAC:
                yield Segment(TAG.DIAC)

            if tag == TAG.BASE_DIS and config.spaces_wild and i < n-1:
    
                if segmented[i+1][0] == TAG.DIAC:
                    yield Segment(TAG.DIAC, segmented[i+1][1])
                    i += 1
                    if i < n-1:
    
                        if segmented[i+1][0] == TAG.BOUN:
                            if re.match('^%s$' % SearchConfig.SPACE_OPT, segmented[i+1][1]):
                                yield Segment(TAG.BOUN, segmented[i+1][1], exp='(?:%s)*?' % segmented[i+1][1])
                                i += 1
                        else:
                            yield Segment(TAG.BOUN, '', exp=SearchConfig.SPACE_OPT)
    
                elif segmented[i+1][0] == TAG.BOUN:
                    if re.match('^%s$' % SearchConfig.SPACE_OPT, segmented[i+1][1]):
                        yield Segment(TAG.BOUN, segmented[i+1][1], exp='(?:%s)*?' % segmented[i+1][1])
                        i += 1
                else:
                    yield Segment(TAG.BOUN, '', exp=SearchConfig.SPACE_OPT)
        i += 1


def _expand(segments, config, debug=False):
    """ expand segments.

    Args:
        segments (generator): sequence of Segment objects.
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.

    Yield:
        Segment: expanded segments.

    """
    for segment in segments:

        if config.exact:
            segment.exp = segment.ori

        else:

            ## RASM ##

            if config.rasm and segment.tag in (TAG.BASE_CON, TAG.BASE_DIS):
                if segment.end:
                    segment.exp = config.RASM_NYQ_MAPPING.get(segment.ori, config.RASM_MAPPING.get(segment.ori, segment.ori)) #FIXME

                else:
                    segment.exp = config.RASM_MAPPING.get(segment.ori, segment.ori)
            
            else:
            
                ## TAG.DIAC ##

                if segment.tag == TAG.DIAC:
    
                    if config.ignore_diac:
                        segment.exp = SearchConfig.DIAC_OPT
    
                    else:
    
                        if config.diac_expansion:
                            if config.diac_synonym:
                                segment.exp = SearchConfig.DIAC_EXP_MAPPING.get(segment.ori, SearchConfig.DIAC_OPT if not segment.ori else \
                                            '%s%s' % (SearchConfig.DIAC_OPT,
                                                ''.join(c for s in ((seg, SearchConfig.DIAC_OPT) for seg in segment.ori) for c in s)))
    
                            else:
                                segment.exp = SearchConfig.DIAC_OPT if not segment.ori else \
                                '%s%s' % (SearchConfig.DIAC_OPT, ''.join(c for s in ((seg, SearchConfig.DIAC_OPT) for seg in segment.ori) for c in s))
    
                        else:
                            if config.diac_synonym:
                                segment.exp = SearchConfig.DIAC_MAPPING.get(segment.ori, segment.ori)
                            else:
                                segment.exp = segment.ori
    
                ## TAG.BASE ##
    
                elif segment.tag in (TAG.BASE_CON, TAG.BASE_DIS):
                    if segment.end:
                        segment.exp = config.base_dis_mapping.get(segment.ori, config.base_con_mapping.get(segment.ori, segment.ori))
                    else:
                        segment.exp = config.base_con_mapping.get(segment.ori, config.base_dis_mapping.get(segment.ori, segment.ori))
        
                ### TAG.BOUN ##
                
                # clusters have not been expanded before
                elif not segment.exp:
                    segment.exp = segment.ori

        if segment.tag is not TAG.BOUN:
            segment.exp += config.OPT_NOTEXT
        
        yield segment

def process_regex(pattern, config, debug=False):
    """ Expand groups of literals within pattern and yield everything.
    
    Args:
        pattern (str): pattern process.
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.
    
    Yield:
        Segment: expanded segment.
    
    """
    substr = [(m.start(), m.end(), _expand(_segment(m.group().replace('\\', ''), config, debug), config, debug)) # remove escaping
              for m in config.REGEX_REGEX.finditer(pattern)]    

    expanded_substr = {ini : exp for ini, _, exp in substr}
    lengths_substr = {ini : (end - ini) for ini, end, _ in substr}

    size = len(pattern)

    i = 0
    while i < size:
        if i in expanded_substr:
            yield from expanded_substr[i]
            i += lengths_substr[i]

        else:
            yield Segment(TAG.BOUN, ori=pattern[i], ini=False, end=False, exp=pattern[i])

            if pattern[i] == ')' and (i == size-1 or pattern[i+1] not in '?+*'):
                yield Segment(TAG.DIAC, exp = SearchConfig.DIAC_OPT)

            if pattern[i] in '+*' and (i > 0 and pattern[i-1] == ')') and (i == size-1 or pattern[i+1] not in '?+*'):
                yield Segment(TAG.DIAC, exp = SearchConfig.DIAC_OPT)

            if pattern[i] == '?' and (i > 1 and pattern[i-1] in '?+*' and pattern[i-2] == ')'):
                yield Segment(TAG.DIAC, exp = SearchConfig.DIAC_OPT)

            i += 1

def fix_offsets(result, repl, loc, config=SearchConfig(), debug=False):
    """ Update the initial and final position of the results of the search in case there are clusters in the text.

    Args:
        result (list): results of a pattern search. Struct:
            [ (pat1, ini1.1, end1.1), (pat1, ini1,2, end1.2)], [(pat2, ...), ...]
        repl (dict): pairs of replaced char and equivalent text.
        loc (list): tuples containing position of replaced char (int) and char (str).
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.

    Return:
        str, int, int: pattern found and corrected ini and end offsets.

    """
    if debug:
        print('\nfix_offsets (1) result =', result, file=sys.stderr) #TRACE
        print('fix_offsets (2) repl   =', repl, file=sys.stderr) #TRACE
        print('fix_offsets (3) loc    =', loc, file=sys.stderr) #TRACE

    loc_info = []
    i = 0
    while i < len(loc):

        j, clus = loc[i]
        acc_gap = sum(len(repl[loc[k][1]])-1 for k in range(i))
        ini_adjusted = j + acc_gap
        #end_adjusted = ini_adjusted+len(repl[clus].strip(nginx.DIAC)) #FIXME add vowel strip
        end_adjusted = ini_adjusted+len(repl[clus]) #FIXME add vowel strip

        loc_info.append((clus, repl[clus], ini_adjusted, end_adjusted, end_adjusted-ini_adjusted-1))
        i += 1

    if debug:
        print('fix_offsets (4) loc    =', loc_info, file=sys.stderr) #TRACE

    for tok, ini, end in result:
        for clus, decomp, ini_clus, end_clus, gap in loc_info[::-1]:

            if ini > ini_clus and ini <= end_clus:
                if debug: print(f'fix_offsets (A) ini={ini} ; ini_clus={ini_clus}', file=sys.stderr) #TRACE
                ini = ini_clus

            if end > ini_clus and end <= end_clus:
                if debug: print(f'fix_offsets (B) end={end} ; ini_clus={ini_clus}', file=sys.stderr) #TRACE
                end = ini_clus+1

            if ini > end_clus:
                if debug: print(f'fix_offsets (C) ini={ini} ; gap={gap}', file=sys.stderr) #TRACE
                ini -= gap

            if end > end_clus:
                if debug: print(f'fix_offsets (D) end={end} ; gap={gap}', file=sys.stderr) #TRACE
                end -= gap

        yield tok, ini, end

def _search(text, pattern, config, debug=False):
    """ search pattern in text.

    Args:
        text (str): text to search.
        pattern (str): pattern to search in text.
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.

    Yield:
        str, int, int: matched subtext, initial offset, end offset.

    """
    if config.regex:
        expanded = process_regex(pattern, config, debug)       

    else:
        expanded = _expand(_segment(pattern, config, debug), config, debug)

    if debug:
        expanded, expanded_ = tee(expanded)
        for segment in expanded_:
            print('@debug@', segment, file=sys.stderr) #TRACE

    repl = ''.join(segment.exp for segment in expanded)

    if debug:
        print('::repl::', repl, file=sys.stderr) #TRACE
        print('::text::', text, file=sys.stderr) #TRACE

    for m in re.compile(repl).finditer(text):
        tok, ini, end = m.group(), *m.span()
    
        if tok:
            yield tok, ini, end

def rasmise(text, config=SearchConfig(), debug=False):
    """ replace base graphemes by archigraphemes in text.

    Args:
        config (SearchConfig): configuration of search.
        debug (bool): debug mode.

    Yield:
        str: each modified char from text

    """    
    n = len(text)
    i = 0

    while i < n:
        char = text[i]
        if char in config.RASM_NYQ_MAPPING:
            j = i+1
            yielded = False
            while j < n:
                if text[j] in config.base_con_mapping or text[j] in config.base_dis_mapping:
                    yield config.RASM_MAPPING[char]
                    yielded = True
                    break
                elif text[j] not in config.DIAC:
                    yielded = True
                    yield config.RASM_NYQ_MAPPING[char]
                    break
                j+=1

            if not yielded:
                yield config.RASM_NYQ_MAPPING[char]

        elif char in config.RASM_MAPPING:
            yield config.RASM_MAPPING[char]
        else:
            yield char
        i += 1

def letterblock_split(rasmised_text, ignore_alif=True, config=SearchConfig()):
    """ split rasmised_text into letterblocks, keeping only the rasm characters and
    the indexes to the original text.

    Args:
        rasmised_text (str): rasmised text.
        ignore_alif (bool): do not keep alif in rasm.
        config (SearchConfig): configuration of search.

    Yield:
        (int, int, str): initial offset, end offset, rasmised letterblock.

    """
    lb_rasm = []

    i = ini = 0
    n = len(rasmised_text)

    while i < n:
        char = rasmised_text[i]
    
        if char in 'اردو':

            if ignore_alif and char == 'ا':
                if lb_rasm:
                    yield ini, i, ''.join(lb_rasm)
                    lb_rasm = []
                    ini = i+1
                else:
                    ini = i
            else:
                lb_rasm.append(char)
                yield ini, i+1, ''.join(lb_rasm)
                lb_rasm = []

                ini = i+1

        elif char in SearchConfig.SPACE:
            if lb_rasm:
                yield ini, i, ''.join(lb_rasm)
                lb_rasm = []
            
            ini = i+1

        elif char in config.RASM_LETTERS:
            lb_rasm.append(char)

        i += 1

    if lb_rasm:
        yield ini, n, ''.join(lb_rasm)

def search(text, patterns, config=None, config_params=None, preproc=False, debug=False):
    """ search pattern in text.

    Args:
        text (str): text to search.
        patterns (list): patterns to search in text.
        config (SearchConfig): Configuration object.
        config_params (dict): parameters to instanciate a search configuration object.
        preproc (bool): make preprocessing of archigraphemic letterblocks before search.
        debug (bool): debug mode.

    Yield:
        str, list: pattern and sequence of tuples containing matched string, ini offset, end offset.

    """
    if not config:
        if not config_params:
            config = SearchConfig()
        else:
            config = SearchConfig(config_params)

    #
    # preprocess clusters and rasm
    #

    if config.cluster_enable:

        clusters = set(char for char in config.clusters_text if char in text)

        if not clusters:
            config.cluster_enable = False

        else:
            clusters_repl = {clus : ''.join(s.ori for s in config.clusters_text[clus]) for clus in clusters}

            regex_clus = re.compile('|'.join(clusters_repl))

            clusters_loc = [(m.start(), m.group()) for m in regex_clus.finditer(text)]

            patterns = [regex_clus.sub(lambda m: clusters_repl[m.group(0)], pat) for pat in patterns]
            text = regex_clus.sub(lambda m: clusters_repl[m.group(0)], text)
                            
    if config.rasm:
       
        #FIXME for now add spaces to NQY if missing, later an error msg should be sent to an interface
        #patterns = (re.sub(r'([NQY])(?! |$)', r'\1 ', pat) for pat in patterns) 

        # preprocess archigraphemes in Latin script
        regex_rasm = re.compile('|'.join(config.RASM_TRANSLIT))
        patterns = [regex_rasm.sub(lambda m: config.RASM_TRANSLIT[m.group(0)], pat) for pat in patterns]

        # modify text
        text = ''.join(rasmise(text, config, debug))

        if debug:
            print('::modified text::', text, file=sys.stderr) #TRACE

    if config.ignore_alif:

        patterns = [pat.translate({ord(c) : None for c in SearchConfig.ALIF}) for pat in patterns]

        alifs_loc = [(i, c) for i, c in enumerate(text) if c in SearchConfig.ALIF]
        alifs_repl = {c : '' for i, c in alifs_loc}

        text = text.translate({ord(c) : None for c in SearchConfig.ALIF})

    if debug:
        print(config, file=sys.stderr) #TRACE

    #
    # apply search
    #

    if not preproc:
        results = [list(_search(text, pat, config, debug)) for pat in patterns]

    else:
        # $ make install && echo "و شيء الله فوبر اال اسيسي هعاس ها شيء لله فوبر فف ناشس" | yakabikaj search --preproc --pattern "شيء  الله فوبر" "خصي غلشوغ" "يء الله هفو"
        # [(4, [
        #        ('شيء  الله فوبر', [(0, 3, 'سی'), (5, 9, 'لله'), (10, 12, 'ڡو', (12, 14, 'ٮر')])
        #      ]
        #  ), 
        #  (3, [
        #       ('خصي غلشوغ', [(0, 3, حصی'), (4, 8, 'علسو'), (8, 9, 'ع',)]),
        #       ('يء الله هفو', [(0, 2, 'ی'), (3, 7, 'لله'), (8, 11, 'ڡو')])
        #      ]
        #  )
        # ]
        patterns_splitted = ((k, list((i[1], i[2]) for i in gr)) for k, gr in
            groupby(((len(gr), pat, gr) for pat, gr in ((pat, list(letterblock_split(''.join(rasmise(pat)), config))) for pat in patterns)), key=lambda x: x[0]))

        # [ ( 4, [
        #         ('سی الله ڡو ٮر', 'شيء لله فو بر')
        #        ]
        #   ),
        #   ( 3, [
        #          ('حصی علسو ع', 'خصي غلشو غ'),
        #          ('ی الله هڡو', 'يء لله هفو')
        #        ]
        #    )
        #  ]
        patterns_ngrams = [(n, [(' '.join(i[2] for i in lbs), ori) for ori, lbs in grs]) for n, grs in patterns_splitted]        

        text_splitted = list(letterblock_split(''.join(rasmise(text)) if not config.rasm else text, config))

        results = []
        for nletterblock, patterns_gr in patterns_ngrams:
        
            # [ ( (0, 1, 'و'),
            #     (2, 5, 'سی'),
            #     (6, 10, 'لله'),
            #     (11, 13, 'ڡو')
            #   ),
            #  ...
            # ]
            text_overlap_ngrams = list(zip(*[islice(text_splitted, i, None) for i in range(nletterblock)]))
            
            # { 'و سی لله ڡو' : [('و شيء الله فو', 0, 13)), ... ], ... }
            text_ngrams = {}

            for ngram_info in text_overlap_ngrams:

                lbs = ' '.join(i[2] for i in ngram_info)
                info = (text[ngram_info[0][0]:ngram_info[-1][1]], ngram_info[0][0], ngram_info[-1][1])
                text_ngrams[lbs] = text_ngrams.get(lbs, [])+[info]


            if debug: #TRACE
                from pprint import pprint #TRACE
                print('@debug@ patterns_ngrams =', end='', file=sys.stderr) #TRACE
                pprint(patterns_ngrams, stream=sys.stderr) #TRACE
                print('@debug@ text_grams =', end='', file=sys.stderr) #TRACE
                pprint(text_ngrams, stream=sys.stderr) #TRACE
        
            res = []
            for rasm_pat, ori_pat in patterns_gr:
                
                if rasm_pat in text_ngrams:

                    if debug:
                        print('@debug@ rasm_pat found in text_ngrams', file=sys.stderr) #TRACE
                    
                    for ori_text, ini, end in text_ngrams[rasm_pat]:

                        res_ = list(_search(ori_text, ori_pat, config, debug))
                        if res_:
                            res.append((res_[0][0], ini, end) if res_ else [])

            results.append(res if res else [])

    #
    # post-process clusters and alifs
    #

    if config.cluster_enable:
        results = (list(fix_offsets(res, clusters_repl, clusters_loc, config, debug)) for res in results)

    if config.ignore_alif:
        #FIXME
        pass #DEBUG
        #results = (list(fix_offsets(res, alifs_repl, alifs_loc, nginx, debug)) for res in results) #DEBUG

    yield from zip(patterns, results)


