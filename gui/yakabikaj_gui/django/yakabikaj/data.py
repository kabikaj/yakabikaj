#!/usr/bin/env python3
#
#    data.py
#
# data for search algorihm
#
#                        |            |     _)  |           _) 
#      |   |   _` |      |  /   _` |  __ \   |  |  /   _` |  | 
#      |   |  (   |        <   (   |  |   |  |    <   (   |  | 
#     \__, | \__,_|     _|\_\ \__,_| _.__/  _| _|\_\ \__,_|  |        احفظ هذا المصودر يا كبيكج يا كبيكج
#     ____/                                              ___/  
#
#
# This code is licensed under GPLv3
# Copyright (C) 2021 Alicia González Martínez and Thomas Milo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>
#
##########################################################################################################

import re
import sys
from enum import Enum
from collections import OrderedDict


class TAG(Enum):
    """ types of segments in pattern.

    """
    BASE_CON = 1
    BASE_DIS = 2
    DIAC     = 3
    CLUS     = 4
    BOUN     = 5

class Segment:
    """ Character or cluster (group of characters) yielded after pattern segmentation.

    """
    def __init__(self, tag, ori='', ini=False, end=False, exp=None):
        """ Constructor.

        Attributes:
            tag (TAG): type of segment.
            ori (str): original string in segment.
            exp (str): expanded segment.
            ini (bool): indicate if segment is at the beginning of a letterblock.
            end (bool): indicate if segment is at the end of a letterblock.

        """
        self.tag = tag
        self.ori = ori
        self.exp = exp
        self.ini = ini
        self.end = end

    def __str__(self):
        """ Info to print for debugging.

        """
        return 'Segment(tag={0:<12}, ini={1!s:<8}, end={2!s:<8}, ori={3!s:<4}, exp="{4}")'.format(
                self.tag, self.ini, self.end, self.ori, self.exp)

class SearchConfig:
    """ Configuration for search.

    Class attributes:
        SPACE (str): list of characters that can be spaces.
        SPACE_OPT (str): regex for posible spaces.
        DIAC (str): inventory of diacritics.
        DIAC_OPT (str):  regex for diacritic clusters.
        OPT_NOTEXT (str): regex for tatweel and invisible unicodes.
        REGEX_REGEX (re.Pattern): regex for catching sequences of literals in a regex.
        NUM_PUNCT (dict): inventory of number and punctuation equivalences.
        DIAC_EXP_MAPPING (dict): inventory of expanded diacritic mappings.
        DIAC_MAPPING (dict): inventory of diacritic mappings without expansion.
        _BASE_CON_MAPPING (dict): inventory of single or cluster base characters that connect and their mappings.
        _BASE_DIS_MAPPING (dict): inventory of single or cluster base characters that do not connect and their mappings.
        RASM_NYQ_MAPPING (dict): inventory of mappings for NQY archigraphemes.
        RASM_MAPPING (dict): inventory of graphemic to archigraphemic mappings expext NQY.
        RASM_LETTERS (dict_values): inventory of rasm characters in Arabic script.
        CLUSTERS (dict): inventory of clusters in unicode.

    """
    SPACE = ' \t\n\r\x0b\x0c  '
    SPACE_OPT = r'(?:%s)*' % '|'.join(SPACE)
    #SPACE_OPT = r'(?: |\t|\n|\r|\x0b|\x0c| | )*' #FIXME

    DIAC = 'ًࣰٌࣱٍࣲَُِّْٰۭ۪ٓۡۢ۟۠۫'

    DIAC_OPT = r'(?:%s)*?' % '|'.join(DIAC)

    OPT_NOTEXT = r'(?:ـ|‌|‍)*?'  # 640, 200C, 200D

    REGEX_REGEX = None
    
    NUM_PUNCT = { '۱' : '(?:۱|1)' ,  # numbers
                  '1' : '(?:۱|1)' ,
                  '۲' : '(?:۲|2)' ,
                  '2' : '(?:۲|2)' ,
                  '۳' : '(?:۳|3)' ,
                  '3' : '(?:۳|3)' ,
                  '۴' : '(?:۴|٤|4)' ,
                  '٤' : '(?:۴|٤|4)' ,
                  '4' : '(?:۴|٤|4)' ,
                  '۵' : '(?:۵|٥|5)' ,
                  '٥' : '(?:۵|٥|5)' ,
                  '5' : '(?:۵|٥|5)' ,
                  '۶' : '(?:۶|٦|6)' ,
                  '٦' : '(?:۶|٦|6)' ,
                  '6' : '(?:۶|٦|6)' ,
                  '۷' : '(?:۷|7)' ,
                  '7' : '(?:۷|7)' ,
                  '۸' : '(?:۸|8)' ,
                  '8' : '(?:۸|8)' ,
                  '۹' : '(?:۹|9)' ,
                  '9' : '(?:۹|9)' ,
                  '،' : '(?:،|,)' , # punctuation
                  ',' : '(?:،|,)' , 
                  '؛' : '(?:؛|;)' , 
                  ';' : '(?:؛|;)' ,
    }

    DIAC_EXP_MAPPING = OrderedDict({
        ''     :  DIAC_OPT ,
        'َّٰٓ' : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٰ{D}ٓ{D}'.format(D=DIAC_OPT) ,
        'َّٰٓ' : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٰ{D}ٓ{D}'.format(D=DIAC_OPT) ,
        'َّٰ'  : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٰ{D}'.format(D=DIAC_OPT) ,
        'َّٰ'  : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٰ{D}'.format(D=DIAC_OPT) ,
        'َّٓ'  : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٓ{D}'.format(D=DIAC_OPT) ,
        'َّٓ'  : r'{D}(?:ّ{D}َ|َ{D}ّ){D}ٓ{D}'.format(D=DIAC_OPT) ,
        'َّۢ'  : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'َّۢ'  : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ََّ'  : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ََّ'  : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ُّۢ'  : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ُّۢ'  : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ُُّ'  : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ُُّ'  : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ِّۭ'  : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ِّۭ'  : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ِِّ'  : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ِِّ'  : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'َّ'   : r'{D}(?:ّ{D}َ|َ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'َّ'   : r'{D}(?:ّ{D}َ|َ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ّٰ'   : r'{D}(?:ّ{D}َ|َ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ّٰ'   : r'{D}(?:ّ{D}َ|َ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ُّ'   : r'{D}(?:ّ{D}ُ|ُ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ُّ'   : r'{D}(?:ّ{D}ُ|ُ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ِّ'   : r'{D}(?:ّ{D}ِ|ِ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ِّ'   : r'{D}(?:ّ{D}ِ|ِ{D}ّ){D}'.format(D=DIAC_OPT) ,
        'ًّ'   : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ًّ'   : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ࣰّ'   : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ࣰّ'   : r'{D}(?:(?:ّ{D}(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ٌّ'   : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ٌّ'   : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ࣱّ'   : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'ࣱّ'   : r'{D}(?:(?:ّ{D}(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُ{D}ّۢ)){D}'.format(D=DIAC_OPT) ,
        'َۢ'   : r'{D}(?:ً|َ{D}َ|ࣰ|َۢ){D}'.format(D=DIAC_OPT) ,
        'ََ'   : r'{D}(?:ً|َ{D}َ|ࣰ|َۢ){D}'.format(D=DIAC_OPT) ,
        'ُۢ'   : r'{D}(?:ٌ|ُ{D}ُ|ࣱ|ُۢ){D}'.format(D=DIAC_OPT) ,
        'ُُ'   : r'{D}(?:ٌ|ُ{D}ُ|ࣱ|ُۢ){D}'.format(D=DIAC_OPT) ,
        'ِۭ'   : r'{D}(?:ٍ|ِ{D}ِ|ࣲ|ِۭ){D}'.format(D=DIAC_OPT) ,
        'ِِ'   : r'{D}(?:ٍ|ِ{D}ِ|ࣲ|ِۭ){D}'.format(D=DIAC_OPT) ,
        'ٍّ'   : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ٍّ'   : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ࣲّ'   : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ࣲّ'   : r'{D}(?:(?:ّ{D}(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِ{D}ّۭ)){D}'.format(D=DIAC_OPT) ,
        'ً'    : r'{D}(?:ً|َ{D}َ|ࣰ|َۢ){D}'.format(D=DIAC_OPT) ,
        'ࣰ'    : r'{D}(?:ً|َ{D}َ|ࣰ|َۢ){D}'.format(D=DIAC_OPT) ,
        'ٌ'    : r'{D}(?:ٌ|ُ{D}ُ|ࣱ|ُۢ){D}'.format(D=DIAC_OPT) ,
        'ࣱ'    : r'{D}(?:ٌ|ُ{D}ُ|ࣱ|ُۢ){D}'.format(D=DIAC_OPT) ,
        'ٍ'    : r'{D}(?:ٍ|ِ{D}ِ|ࣲ|ِۭ){D}'.format(D=DIAC_OPT) ,
        'ࣲ'    : r'{D}(?:ٍ|ِ{D}ِ|ࣲ|ِۭ){D}'.format(D=DIAC_OPT) ,
        'ۡ'    : r'{D}(?:ۡ|ْ){D}'.format(D=DIAC_OPT) ,
        'ْ'    : r'{D}(?:ۡ|ْ){D}'.format(D=DIAC_OPT) ,
    })

    DIAC_MAPPING = OrderedDict({
        'َّٰٓ': r'{D}(?:َّ|َّ)ٰٓ' ,
        'َّٰٓ' : r'(?:َّ|َّ)ٰٓ' ,
        'َّٰ'  : r'(?:َّ|َّ)ٰ' ,
        'َّٰ'  : r'(?:َّ|َّ)ٰ' ,
        'َّٓ'  : r'(?:َّ|َّ)ٓ' ,
        'َّٓ'  : r'(?:َّ|َّ)ٓ' ,
        'َّۢ'  : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'َّۢ'  : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ََّ'  : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ََّ'  : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ُّۢ'  : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ُّۢ'  : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ُُّ'  : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ُُّ'  : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ِّۭ'  : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ِّۭ'  : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ِِّ'  : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ِِّ'  : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'َّ'   : r'(?:َّ|َّ)' ,
        'َّ'   : r'(?:َّ|َّ)' ,
        'ّٰ'   : r'(?:َّ|َّ)' ,
        'ّٰ'   : r'(?:َّ|َّ)' ,
        'ُّ'   : r'(?:ُّ|ُّ)' ,
        'ُّ'   : r'(?:ُّ|ُّ)' ,
        'ِّ'   : r'(?:ِّ|ِّ)' ,
        'ِّ'   : r'(?:ِّ|ِّ)' ,
        'ًّ'   : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ًّ'   : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ࣰّ'   : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ࣰّ'   : r'(?:(?:ّ(?:ً|ََ|ࣰ|َۢ)|(?:ً|ََ|ࣰ)ّ|َّۢ))' ,
        'ٌّ'   : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ٌّ'   : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ࣱّ'   : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'ࣱّ'   : r'(?:(?:ّ(?:ٌ|ُُ|ࣱ|ُۢ)|(?:ٌ|ُُ|ࣱ)ّ|ُّۢ))' ,
        'َۢ'   : r'(?:ً|ََ|ࣰ|َۢ)' ,
        'ََ'   : r'(?:ً|ََ|ࣰ|َۢ)' ,
        'ُۢ'   : r'(?:ٌ|ُُ|ࣱ|ُۢ)' ,
        'ُُ'   : r'(?:ٌ|ُُ|ࣱ|ُۢ)' ,
        'ِۭ'   : r'(?:ٍ|ِِ|ࣲ|ِۭ)' ,
        'ِِ'   : r'(?:ٍ|ِِ|ࣲ|ِۭ)' ,
        'ٍّ'   : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ٍّ'   : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ࣲّ'   : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ࣲّ'   : r'(?:(?:ّ(?:ٍ|ِِ|ࣲ|ِۭ)|(?:ٍ|ِِ|ࣲ)ّ|ِّۭ))' ,
        'ً'   : r'(?:ً|ََ|ࣰ|َۢ)' ,
        'ࣰ'   : r'(?:ً|ََ|ࣰ|َۢ)' ,
        'ٌ'   : r'(?:ٌ|ُُ|ࣱ|ُۢ)' ,
        'ࣱ'   : r'(?:ٌ|ُُ|ࣱ|ُۢ)' ,
        'ٍ'   : r'(?:ٍ|ِِ|ࣲ|ِۭ)' ,
        'ࣲ'   : r'(?:ٍ|ِِ|ࣲ|ِۭ)' ,
        'ۡ'   : r'(?:ۡ|ْ)' ,
        'ْ'   : r'(?:ۡ|ْ)' ,
    })

    _BASE_CON_MAPPING = OrderedDict({
    
        # START COMPOSITE

        'صۜ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,
        'صۣ' : r'(?:صۣ|ص|س|س|ﺱ|ﺲ|ﺳ|ﺴ|ۜ|ۣ)' ,
        
        'ىٔ' : r'(?:ئ|ىٔ|ﺉ|ىء|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,
        'ىء' : r'(?:ئ|ىٔ|ﺉ|ىء|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,

        'هٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ھٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ۂ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﻩٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﻪٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﮪٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﮫٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﮦٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,
        'ﮧٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)%sٔ|ۀ|ﮤ|ﮥ|ۂ)' % DIAC_OPT ,

        # END COMPOSITE

        # identical maping added so that they are recognised by the scanner in the segmentation
        'ٮ': r'ٮ' ,
        'ڡ': r'ڡ' ,
        'ٯ': r'ٯ' ,

        'ب' : r'(?:ب|ﺏ|ﺐ|ﺑ|ﺒ)' ,            # U+0628 ARABIC LETTER BEH
        'پ' : r'(?:پ|ﭖ|ﭗ|ﭘ|ﭙ)' ,            # U+067e ARABIC LETTER PEH
        'ت' : r'(?:ت|ﺕ|ﺖ|ﺗ|ﺘ)' ,            # U+062a ARABIC LETTER TEH
        'ث' : r'(?:ث|ﺙ|ﺚ|ﺛ|ﺜ)' ,            # U+062b ARABIC LETTER THEH
        'ج' : r'(?:ج|ﺝ|ﺞ|ﺟ|ﺠ)' ,            # U+062c ARABIC LETTER JEEM
        'ح' : r'(?:ح|ﺡ|ﺢ|ﺣ|ﺤ)' ,            # U+062d ARABIC LETTER HAH
        'خ' : r'(?:خ|ﺥ|ﺦ|ﺧ|ﺨ)' ,            # U+062e ARABIC LETTER KHAH
        'چ' : r'(?:چ|ﭺ|ﭻ|ﭼ|ﭽ)' ,            # U+0686 ARABIC LETTER TCHEH
        'س' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    # U+0633 ARABIC LETTER SEEN
        'ش' : r'(?:ش|ﺵ|ﺶ|ﺷ|ﺸ)' ,            # U+0634 ARABIC LETTER SHEEN
        'ص' : r'(?:ص|ﺹ|ﺺ|ﺻ|ﺼ)' ,            # U+0635 ARABIC LETTER SAD
        'ض' : r'(?:ض|ﺽ|ﺾ|ﺿ|ﻀ)' ,            # U+0636 ARABIC LETTER DAD
        'ط' : r'(?:ط|ﻁ|ﻂ|ﻃ|ﻄ)' ,            # U+0637 ARABIC LETTER TAH
        'ظ' : r'(?:ظ|ﻅ|ﻆ|ﻇ|ﻈ)' ,            # U+0638 ARABIC LETTER ZAH
        'ع' : r'(?:ع|ﻉ|ﻊ|ﻋ|ﻌ)' ,            # U+0639 ARABIC LETTER AIN
        'غ' : r'(?:غ|ﻍ|ﻎ|ﻏ|ﻐ)' ,            # U+063a ARABIC LETTER GHAIN
        'ف' : r'(?:ف|ﻑ|ﻒ|ﻓ|ﻔ)' ,            # U+0641 ARABIC LETTER FEH
        'ق' : r'(?:ق|ﻕ|ﻖ|ﻗ|ﻘ)' ,            # U+0642 ARABIC LETTER QAF
        'ك' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+0643 ARABIC LETTER KAF
        'ک' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+06a9 ARABIC LETTER KEHEH
        'گ' : r'(?:گ|ﮒ|ﮓ|ﮔ|ﮕ)' ,            # U+06af ARABIC LETTER GAF
        'ل' : r'(?:ل|ﻝ|ﻞ|ﻟ|ﻠ)' ,            # U+0644 ARABIC LETTER LAM
        'م' : r'(?:م|ﻡ|ﻢ|ﻣ|ﻤ)' ,            # U+0645 ARABIC LETTER MEEM

        'ي' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+064a ARABIC LETTER YEH
        'ی' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+06cc ARABIC LETTER FARSI YEH

        'ى' : r'(?:ى|ﻯ|ﻰ|ي|ی|ﻱ|ﻲ|ﯼ|ﯽ|ے|ﮮ|ﮯ)' ,     # U+0649 ARABIC LETTER ALEF MAKSURA

        'ن' : r'(?:ن|ں|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,       # U+0646 ARABIC LETTER NOON
        'ں' : r'(?:ن|ں|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,       # ں U+06ba ARABIC LETTER NOON GHUNNA ; urdu aspiration

        'ئ' : r'(?:ئ|ىٔ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,   # U+0626 ARABIC LETTER YEH WITH HAMZA ABOVE
        'ﺉ' : r'(?:ئ|ىٔ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,   # U+fe89 ARABIC LETTER YEH WITH HAMZA ABOVE ISOLATED FORM
        'ﺊ' : r'(?:ئ|ىٔ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,   # U+fe8a ARABIC LETTER YEH WITH HAMZA ABOVE FINAL FORM
        'ﺋ' : r'(?:ئ|ىٔ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,   # U+fe8b ARABIC LETTER YEH WITH HAMZA ABOVE INITIAL FORM
        'ﺌ' : r'(?:ئ|ىٔ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ)' ,   # U+fe8c ARABIC LETTER YEH WITH HAMZA ABOVE MEDIAL FORM

        'ﺏ' : r'(?:ب|ﺏ|ﺐ|ﺑ|ﺒ)' ,            # U+fe8f ARABIC LETTER BEH ISOLATED FORM
        'ﺐ' : r'(?:ب|ﺏ|ﺐ|ﺑ|ﺒ)' ,            # U+fe90 ARABIC LETTER BEH FINAL FORM
        'ﺑ' : r'(?:ب|ﺏ|ﺐ|ﺑ|ﺒ)' ,            # U+fe91 ARABIC LETTER BEH INITIAL FORM
        'ﺒ' : r'(?:ب|ﺏ|ﺐ|ﺑ|ﺒ)' ,            # U+fe92 ARABIC LETTER BEH MEDIAL FORM
        'ﭖ' : r'(?:پ|ﭖ|ﭗ|ﭘ|ﭙ)' ,            # U+fb56 ARABIC LETTER PEH ISOLATED FORM
        'ﭗ' : r'(?:پ|ﭖ|ﭗ|ﭘ|ﭙ)' ,            # U+fb57 ARABIC LETTER PEH FINAL FORM
        'ﭘ' : r'(?:پ|ﭖ|ﭗ|ﭘ|ﭙ)' ,            # U+fb58 ARABIC LETTER PEH INITIAL FORM
        'ﭙ' : r'(?:پ|ﭖ|ﭗ|ﭘ|ﭙ)' ,            # U+fb59 ARABIC LETTER PEH MEDIAL FORM
        'ﺕ' : r'(?:ت|ﺕ|ﺖ|ﺗ|ﺘ)' ,            # U+fe95 ARABIC LETTER TEH ISOLATED FORM
        'ﺖ' : r'(?:ت|ﺕ|ﺖ|ﺗ|ﺘ)' ,            # U+fe96 ARABIC LETTER TEH FINAL FORM
        'ﺗ' : r'(?:ت|ﺕ|ﺖ|ﺗ|ﺘ)' ,            # U+fe97 ARABIC LETTER TEH INITIAL FORM
        'ﺘ' : r'(?:ت|ﺕ|ﺖ|ﺗ|ﺘ)' ,            # U+fe98 ARABIC LETTER TEH MEDIAL FORM
        'ﺙ' : r'(?:ث|ﺙ|ﺚ|ﺛ|ﺜ)' ,            # U+fe99 ARABIC LETTER THEH ISOLATED FORM
        'ﺚ' : r'(?:ث|ﺙ|ﺚ|ﺛ|ﺜ)' ,            # U+fe9a ARABIC LETTER THEH FINAL FORM
        'ﺛ' : r'(?:ث|ﺙ|ﺚ|ﺛ|ﺜ)' ,            # U+fe9b ARABIC LETTER THEH INITIAL FORM
        'ﺜ' : r'(?:ث|ﺙ|ﺚ|ﺛ|ﺜ)' ,            # U+fe9c ARABIC LETTER THEH MEDIAL FORM
        'ﺝ' : r'(?:ج|ﺝ|ﺞ|ﺟ|ﺠ)' ,            # U+fe9d ARABIC LETTER JEEM ISOLATED FORM
        'ﺞ' : r'(?:ج|ﺝ|ﺞ|ﺟ|ﺠ)' ,            # U+fe9e ARABIC LETTER JEEM FINAL FORM
        'ﺟ' : r'(?:ج|ﺝ|ﺞ|ﺟ|ﺠ)' ,            # U+fe9f ARABIC LETTER JEEM INITIAL FORM
        'ﺠ' : r'(?:ج|ﺝ|ﺞ|ﺟ|ﺠ)' ,            # U+fea0 ARABIC LETTER JEEM MEDIAL FORM
        'ﺡ' : r'(?:ح|ﺡ|ﺢ|ﺣ|ﺤ)' ,            # U+fea1 ARABIC LETTER HAH ISOLATED FORM
        'ﺢ' : r'(?:ح|ﺡ|ﺢ|ﺣ|ﺤ)' ,            # U+fea2 ARABIC LETTER HAH FINAL FORM
        'ﺣ' : r'(?:ح|ﺡ|ﺢ|ﺣ|ﺤ)' ,            # U+fea3 ARABIC LETTER HAH INITIAL FORM
        'ﺤ' : r'(?:ح|ﺡ|ﺢ|ﺣ|ﺤ)' ,            # U+fea4 ARABIC LETTER HAH MEDIAL FORM
        'ﺥ' : r'(?:خ|ﺥ|ﺦ|ﺧ|ﺨ)' ,            # U+fea5 ARABIC LETTER KHAH ISOLATED FORM
        'ﺦ' : r'(?:خ|ﺥ|ﺦ|ﺧ|ﺨ)' ,            # U+fea6 ARABIC LETTER KHAH FINAL FORM
        'ﺧ' : r'(?:خ|ﺥ|ﺦ|ﺧ|ﺨ)' ,            # U+fea7 ARABIC LETTER KHAH INITIAL FORM
        'ﺨ' : r'(?:خ|ﺥ|ﺦ|ﺧ|ﺨ)' ,            # U+fea8 ARABIC LETTER KHAH MEDIAL FORM    
        'ﭺ' : r'(?:چ|ﭺ|ﭻ|ﭼ|ﭽ)' ,            # U+fb7a ARABIC LETTER TCHEH ISOLATED FORM
        'ﭻ' : r'(?:چ|ﭺ|ﭻ|ﭼ|ﭽ)' ,            # U+fb7b ARABIC LETTER TCHEH FINAL FORM
        'ﭼ' : r'(?:چ|ﭺ|ﭻ|ﭼ|ﭽ)' ,            # U+fb7c ARABIC LETTER TCHEH INITIAL FORM
        'ﭽ' : r'(?:چ|ﭺ|ﭻ|ﭼ|ﭽ)' ,            # U+fb7d ARABIC LETTER TCHEH MEDIAL FORM
        'ﺱ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    # U+feb1 ARABIC LETTER SEEN ISOLATED FORM 
        'ﺲ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    # U+feb2 ARABIC LETTER SEEN FINAL FORM    
        'ﺳ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    # U+feb3 ARABIC LETTER SEEN INITIAL FORM  
        'ﺴ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    # U+feb4 ARABIC LETTER SEEN MEDIAL FORM   
        'ۜ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    
        'ۣ' : r'(?:س|ﺱ|ﺲ|ﺳ|ﺴ|صۜ|ۜ|ۣ)' ,    
        'ﺵ' : r'(?:ش|ﺵ|ﺶ|ﺷ|ﺸ)' ,            # U+feb5 ARABIC LETTER SHEEN ISOLATED FORM
        'ﺶ' : r'(?:ش|ﺵ|ﺶ|ﺷ|ﺸ)' ,            # U+feb6 ARABIC LETTER SHEEN FINAL FORM
        'ﺷ' : r'(?:ش|ﺵ|ﺶ|ﺷ|ﺸ)' ,            # U+feb7 ARABIC LETTER SHEEN INITIAL FORM
        'ﺸ' : r'(?:ش|ﺵ|ﺶ|ﺷ|ﺸ)' ,            # U+feb8 ARABIC LETTER SHEEN MEDIAL FORM
        'ﺹ' : r'(?:ص|ﺹ|ﺺ|ﺻ|ﺼ)' ,            # U+feb9 ARABIC LETTER SAD ISOLATED FORM
        'ﺺ' : r'(?:ص|ﺹ|ﺺ|ﺻ|ﺼ)' ,            # U+feba ARABIC LETTER SAD FINAL FORM
        'ﺻ' : r'(?:ص|ﺹ|ﺺ|ﺻ|ﺼ)' ,            # U+febb ARABIC LETTER SAD INITIAL FORM
        'ﺼ' : r'(?:ص|ﺹ|ﺺ|ﺻ|ﺼ)' ,            # U+febc ARABIC LETTER SAD MEDIAL FORM
        'ﺽ' : r'(?:ض|ﺽ|ﺾ|ﺿ|ﻀ)' ,            # U+febd ARABIC LETTER DAD ISOLATED FORM
        'ﺾ' : r'(?:ض|ﺽ|ﺾ|ﺿ|ﻀ)' ,            # U+febe ARABIC LETTER DAD FINAL FORM
        'ﺿ' : r'(?:ض|ﺽ|ﺾ|ﺿ|ﻀ)' ,            # U+febf ARABIC LETTER DAD INITIAL FORM
        'ﻀ' : r'(?:ض|ﺽ|ﺾ|ﺿ|ﻀ)' ,            # U+fec0 ARABIC LETTER DAD MEDIAL FORM
        'ﻁ' : r'(?:ط|ﻁ|ﻂ|ﻃ|ﻄ)' ,            # U+fec1 ARABIC LETTER TAH ISOLATED FORM
        'ﻂ' : r'(?:ط|ﻁ|ﻂ|ﻃ|ﻄ)' ,            # U+fec2 ARABIC LETTER TAH FINAL FORM
        'ﻃ' : r'(?:ط|ﻁ|ﻂ|ﻃ|ﻄ)' ,            # U+fec3 ARABIC LETTER TAH INITIAL FORM
        'ﻄ' : r'(?:ط|ﻁ|ﻂ|ﻃ|ﻄ)' ,            # U+fec4 ARABIC LETTER TAH MEDIAL FORM
        'ﻅ' : r'(?:ظ|ﻅ|ﻆ|ﻇ|ﻈ)' ,            # U+fec5 ARABIC LETTER ZAH ISOLATED FORM
        'ﻆ' : r'(?:ظ|ﻅ|ﻆ|ﻇ|ﻈ)' ,            # U+fec6 ARABIC LETTER ZAH FINAL FORM
        'ﻇ' : r'(?:ظ|ﻅ|ﻆ|ﻇ|ﻈ)' ,            # U+fec7 ARABIC LETTER ZAH INITIAL FORM
        'ﻈ' : r'(?:ظ|ﻅ|ﻆ|ﻇ|ﻈ)' ,            # U+fec8 ARABIC LETTER ZAH MEDIAL FORM
        'ﻉ' : r'(?:ع|ﻉ|ﻊ|ﻋ|ﻌ)' ,            # U+fec9 ARABIC LETTER AIN ISOLATED FORM
        'ﻊ' : r'(?:ع|ﻉ|ﻊ|ﻋ|ﻌ)' ,            # U+feca ARABIC LETTER AIN FINAL FORM
        'ﻋ' : r'(?:ع|ﻉ|ﻊ|ﻋ|ﻌ)' ,            # U+fecb ARABIC LETTER AIN INITIAL FORM
        'ﻌ' : r'(?:ع|ﻉ|ﻊ|ﻋ|ﻌ)' ,            # U+fecc ARABIC LETTER AIN MEDIAL FORM
        'ﻍ' : r'(?:غ|ﻍ|ﻎ|ﻏ|ﻐ)' ,            # U+fecd ARABIC LETTER GHAIN ISOLATED FORM
        'ﻎ' : r'(?:غ|ﻍ|ﻎ|ﻏ|ﻐ)' ,            # U+fece ARABIC LETTER GHAIN FINAL FORM
        'ﻏ' : r'(?:غ|ﻍ|ﻎ|ﻏ|ﻐ)' ,            # U+fecf ARABIC LETTER GHAIN INITIAL FORM
        'ﻐ' : r'(?:غ|ﻍ|ﻎ|ﻏ|ﻐ)' ,            # U+fed0 ARABIC LETTER GHAIN MEDIAL FORM
        'ﻑ' : r'(?:ف|ﻑ|ﻒ|ﻓ|ﻔ)' ,            # U+fed1 ARABIC LETTER FEH ISOLATED FORM
        'ﻒ' : r'(?:ف|ﻑ|ﻒ|ﻓ|ﻔ)' ,            # U+fed2 ARABIC LETTER FEH FINAL FORM
        'ﻓ' : r'(?:ف|ﻑ|ﻒ|ﻓ|ﻔ)' ,            # U+fed3 ARABIC LETTER FEH INITIAL FORM
        'ﻔ' : r'(?:ف|ﻑ|ﻒ|ﻓ|ﻔ)' ,            # U+fed4 ARABIC LETTER FEH MEDIAL FORM
        'ﻕ' : r'(?:ق|ﻕ|ﻖ|ﻗ|ﻘ)' ,            # U+fed5 ARABIC LETTER QAF ISOLATED FORM
        'ﻖ' : r'(?:ق|ﻕ|ﻖ|ﻗ|ﻘ)' ,            # U+fed6 ARABIC LETTER QAF FINAL FORM
        'ﻗ' : r'(?:ق|ﻕ|ﻖ|ﻗ|ﻘ)' ,            # U+fed7 ARABIC LETTER QAF INITIAL FORM
        'ﻘ' : r'(?:ق|ﻕ|ﻖ|ﻗ|ﻘ)' ,            # U+fed8 ARABIC LETTER QAF MEDIAL FORM
        'ﻙ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fed9 ARABIC LETTER KAF ISOLATED FORM
        'ﻚ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+feda ARABIC LETTER KAF FINAL FORM
        'ﻛ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fedb ARABIC LETTER KAF INITIAL FORM
        'ﻜ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fedc ARABIC LETTER KAF MEDIAL FORM
        'ﮎ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fb8e ARABIC LETTER KEHEH ISOLATED FORM
        'ﮏ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fb8f ARABIC LETTER KEHEH FINAL FORM
        'ﮐ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fb90 ARABIC LETTER KEHEH INITIAL FORM
        'ﮑ' : r'(?:ك|ک|ﻙ|ﻚ|ﻛ|ﻜ|ﮎ|ﮏ|ﮐ|ﮑ)' ,  # U+fb91 ARABIC LETTER KEHEH MEDIAL FORM
        'ﮒ' : r'(?:گ|ﮒ|ﮓ|ﮔ|ﮕ)' ,            # U+fb92 ARABIC LETTER GAF ISOLATED FORM
        'ﮓ' : r'(?:گ|ﮒ|ﮓ|ﮔ|ﮕ)' ,            # U+fb93 ARABIC LETTER GAF FINAL FORM
        'ﮔ' : r'(?:گ|ﮒ|ﮓ|ﮔ|ﮕ)' ,            # U+fb94 ARABIC LETTER GAF INITIAL FORM
        'ﮕ' : r'(?:گ|ﮒ|ﮓ|ﮔ|ﮕ)' ,            # U+fb95 ARABIC LETTER GAF MEDIAL FORM
        'ﻝ' : r'(?:ل|ﻝ|ﻞ|ﻟ|ﻠ)' ,            # U+fedd ARABIC LETTER LAM ISOLATED FORM
        'ﻞ' : r'(?:ل|ﻝ|ﻞ|ﻟ|ﻠ)' ,            # U+fede ARABIC LETTER LAM FINAL FORM
        'ﻟ' : r'(?:ل|ﻝ|ﻞ|ﻟ|ﻠ)' ,            # U+fedf ARABIC LETTER LAM INITIAL FORM
        'ﻠ' : r'(?:ل|ﻝ|ﻞ|ﻟ|ﻠ)' ,            # U+fee0 ARABIC LETTER LAM MEDIAL FORM
        'ﻡ' : r'(?:م|ﻡ|ﻢ|ﻣ|ﻤ)' ,            # U+fee1 ARABIC LETTER MEEM ISOLATED FORM
        'ﻢ' : r'(?:م|ﻡ|ﻢ|ﻣ|ﻤ)' ,            # U+fee2 ARABIC LETTER MEEM FINAL FORM
        'ﻣ' : r'(?:م|ﻡ|ﻢ|ﻣ|ﻤ)' ,            # U+fee3 ARABIC LETTER MEEM INITIAL FORM
        'ﻤ' : r'(?:م|ﻡ|ﻢ|ﻣ|ﻤ)' ,            # U+fee4 ARABIC LETTER MEEM MEDIAL FORM
        'ﻥ' : r'(?:ن|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,          # U+fee5 ARABIC LETTER NOON ISOLATED FORM
        'ﻦ' : r'(?:ن|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,          # U+fee6 ARABIC LETTER NOON FINAL FORM
        'ﻧ' : r'(?:ن|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,          # U+fee7 ARABIC LETTER NOON INITIAL FORM
        'ﻨ' : r'(?:ن|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,          # U+fee8 ARABIC LETTER NOON MEDIAL FORM
        'ۨ' : r'(?:ن|ﻥ|ﻦ|ﻧ|ﻨ|ۨ)' ,         # U+06e8 ARABIC SMALL HIGH NOON

        'ﻯ' : r'(?:ى|ﻯ|ﻰ|ي|ی|ﻱ|ﻲ|ﯼ|ﯽ||ﻳ|ﻴ|ۦ|ۧ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,    # U+feef ARABIC LETTER ALEF MAKSURA ISOLATED FORM
        'ﻰ' : r'(?:ى|ﻯ|ﻰ|ي|ی|ﻱ|ﻲ|ﯼ|ﯽ||ﻳ|ﻴ|ۦ|ۧ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,    # U+fef0 ARABIC LETTER ALEF MAKSURA FINAL FORM
    
        'ه' : r'(?:ه|ﻩ|ﻪ|ﻫ|ﻬ)' ,            # U+0647 ARABIC LETTER HEH
        'ﻩ' : r'(?:ه|ﻩ|ﻪ|ﻫ|ﻬ)' ,            # U+fee9 ARABIC LETTER HEH ISOLATED FORM
        'ﻪ' : r'(?:ه|ﻩ|ﻪ|ﻫ|ﻬ)' ,            # U+feea ARABIC LETTER HEH FINAL FORM
        'ﻫ' : r'(?:ه|ﻩ|ﻪ|ﻫ|ﻬ)' ,            # U+feeb ARABIC LETTER HEH INITIAL FORM
        'ﻬ' : r'(?:ه|ﻩ|ﻪ|ﻫ|ﻬ)' ,            # U+feec ARABIC LETTER HEH MEDIAL FORM
    
        # hindustani: aspiration mark to preseding consonants
        'ھ' : r'(?:ھ|ﮪ|ﮫ|ﮬ|ﮭ)' ,            # U+06be ARABIC LETTER HEH DOACHASHMEE
        'ﮪ' : r'(?:ھ|ﮪ|ﮫ|ﮬ|ﮭ)' ,            # U+fbaa ARABIC LETTER HEH DOACHASHMEE ISOLATED FORM
        'ﮫ' : r'(?:ھ|ﮪ|ﮫ|ﮬ|ﮭ)' ,            # U+fbab ARABIC LETTER HEH DOACHASHMEE FINAL FORM
        'ﮬ' : r'(?:ھ|ﮪ|ﮫ|ﮬ|ﮭ)' ,            # U+fbac ARABIC LETTER HEH DOACHASHMEE INITIAL FORM
        'ﮭ' : r'(?:ھ|ﮪ|ﮫ|ﮬ|ﮭ)' ,            # U+fbad ARABIC LETTER HEH DOACHASHMEE MEDIAL FORM
    
        # hindustani: گول ہا - ɦ voiceless glottal fricative, eg. हूँ ہوں
        'ہ' : r'(?:ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,            # U+06c1 ARABIC LETTER HEH GOAL
        'ﮦ' : r'(?:ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,            # U+fba6 ARABIC LETTER HEH GOAL ISOLATED FORM
        'ﮧ' : r'(?:ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,            # U+fba7 ARABIC LETTER HEH GOAL FINAL FORM
        'ﮨ' : r'(?:ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,            # U+fba8 ARABIC LETTER HEH GOAL INITIAL FORM
        'ﮩ' : r'(?:ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,            # U+fba9 ARABIC LETTER HEH GOAL MEDIAL FORM

        'ﻱ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fef1 ARABIC LETTER YEH ISOLATED FORM
        'ﻲ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fef2 ARABIC LETTER YEH FINAL FORM
        'ﻳ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fef3 ARABIC LETTER YEH INITIAL FORM
        'ﻴ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fef4 ARABIC LETTER YEH MEDIAL FORM
        'ۦ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+06e6 ARABIC SMALL YEH ; 6e6 is standalone miniature ya (btw letterblocks) ; 6e7 btw connected letters
        'ﯼ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fbfc ARABIC LETTER FARSI YEH ISOLATED FORM
        'ﯽ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fbfd ARABIC LETTER FARSI YEH FINAL FORM
        'ﯾ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fbfe ARABIC LETTER FARSI YEH INITIAL FORM
        'ﯿ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+fbff ARABIC LETTER FARSI YEH MEDIAL FORM

        'ۧ' : r'(?:ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ|ے|ﮮ|ﮯ)' ,  # U+06e7 ARABIC SMALL HIGH YEH
    })
    
    _BASE_DIS_MAPPING = OrderedDict({
    
        # START COMPOSITE
        
        'َٰ'  : r'(?:َٰ|َا)' ,
        'َا'   : r'(?:َٰ|َا|َوٰ)' ,
        'َوٰ' : r'(?:َا|َوٰ)' ,
        'آ' : r'(?:آ|آ|ﺁ|ﺂ)' ,
        'أ' : r'(?:أ|أ?|ﺃ|ﺄ)' ,
        'إ' : r'(?:إ|إ?|ﺇ|ﺈ)' ,
        'ٔا' : r'(?:ٔا|ٵ)' ,
        'ا۬' : r'(?:أ|ا(?:۬|ٔ)|ﺃ|ﺄ)' ,
        'ؤ' : r'(?:ؤ|ؤ|ﺅ|ﺆ)' ,

        # END COMPOSITE

        'ء'  : r'(?:ء|ٕ|ٔ|۬)' ,
        'ٕ'  : r'(?:ء|ٕ|ٔ|۬)' ,
        'ٔ'  : r'(?:ء|ٕ|ٔ|۬)' ,
        '۬'  : r'(?:ء|ٕ|ٔ|۬)' ,   # U+06ec ARABIC ROUNDED HIGH STOP WITH FILLED CENTRE ; نقطة مدوّرة مسۡدودة الوسط ; ءَا۬عۡجَمِىࣱّ 41:44:9

        'ا' : r'(?:ا|آ|أ|إ|ٰ|ٱ|ﺍ|ﺎ|ﭐ|ﭑ|ﺁ|ﺂ|ﺃ|ﺄ|ﺇ|ﺈ)' ,                     # U+0627 ARABIC LETTER ALEF

        'و' : r'(?:و|ﻭ|ﻮ|ۥ)' ,                                              # U+0648 ARABIC LETTER WAW
        'ر' : r'(?:ر|ﺭ|ﺮ)' ,                                               # U+0631 ARABIC LETTER REH
        'أ' : r'(?:أ|أ?|ﺃ|ﺄ)' ,                                           # U+0623 ARABIC LETTER ALEF WITH HAMZA ABOVE
        'د' : r'(?:د|ﺩ|ﺪ)' ,                                               # U+062f ARABIC LETTER DAL
        'ة' : r'(?:ة|ﺓ|ﺔ|ۃ)' ,                                             # U+0629 ARABIC LETTER TEH MARBUTA        
        'إ' : r'(?:إ|إ?|ﺇ|ﺈ)' ,                                           # U+0625 ARABIC LETTER ALEF WITH HAMZA BELOW
        'ذ' : r'(?:ذ|ﺫ|ﺬ)' ,                                               # U+0630 ARABIC LETTER THAL
        'ز' : r'(?:ز|ﺯ|ﺰ)' ,                                               # U+0632 ARABIC LETTER ZAIN
        'آ' : r'(?:آ|آ|ﺁ|ﺂ)' ,                                            # U+0622 ARABIC LETTER ALEF WITH MADDA ABOVE
        'ؤ' : r'(?:ؤ|ؤ|ﺅ|ﺆ)' ,                                            # U+0624 ARABIC LETTER WAW WITH HAMZA ABOVE
        'ء' : r'(?:ء|ٔ)' ,                                                 # U+0621 ARABIC LETTER HAMZA
        
        'ے' : r'(?:ے|ﮮ|ﮯ|ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ)' ,                     # U+06d2 ARABIC LETTER YEH BARREE
        'ﮮ' : r'(?:ے|ﮮ|ﮯ|ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ)' ,                     # U+fbae ARABIC LETTER YEH BARREE ISOLATED FORM
        'ﮯ' : r'(?:ے|ﮮ|ﮯ|ي|ی|ى|ﻱ|ﻲ|ﻳ|ﻴ|ۦ|ۧ|ﯼ|ﯽ|ﯾ|ﯿ)' ,                     # U+fbaf ARABIC LETTER YEH BARREE FINAL FORM

        'ۓ' : r'(?:ۓ|ﮰ|ﮱ|ئ|ىٔ|ﺉ|ﺊ|ﺋ|ﺌ)' ,                                  # U+06d3 ARABIC LETTER YEH BARREE WITH HAMZA ABOVE
        'ﮰ' : r'(?:ۓ|ﮰ|ﮱ|ئ|ىٔ|ﺉ|ﺊ|ﺋ|ﺌ)' ,                                  # U+fbb0 ARABIC LETTER YEH BARREE WITH HAMZA ABOVE ISOLATED FORM
        'ﮱ' : r'(?:ۓ|ﮰ|ﮱ|ئ|ىٔ|ﺉ|ﺊ|ﺋ|ﺌ)' ,                                  # U+fbb1 ARABIC LETTER YEH BARREE WITH HAMZA ABOVE FINAL FORM

        'ژ' : r'(?:ژ|ﮊ|ﮋ)' ,                                               # U+0698 ARABIC LETTER JEH
        'ٱ' : r'(?:ا|ٱ|ﺍ|ﺎ|ﭐ|ﭑ)' ,                                         # U+0671 ARABIC LETTER ALEF WASLA
        'ﺍ' : r'(?:ا|آ|أ|إ|ٱ|ﺍ|ﺎ|ﭐ|ﭑ|ﺁ|ﺂ|ﺃ|ﺄ|ﺇ|ﺈ)' ,                       # U+fe8d ARABIC LETTER ALEF ISOLATED FORM
        'ﺎ' : r'(?:ا|آ|أ|إ|ٱ|ﺍ|ﺎ|ﭐ|ﭑ|ﺁ|ﺂ|ﺃ|ﺄ|ﺇ|ﺈ)' ,                       # U+fe8e ARABIC LETTER ALEF FINAL FORM
        'ﭐ' : r'(?:ا|ٱ|ﺍ|ﺎ|ﭐ|ﭑ)' ,                                         # U+fb50 ARABIC LETTER ALEF WASLA ISOLATED FORM
        'ﭑ' : r'(?:ا|ٱ|ﺍ|ﺎ|ﭐ|ﭑ)' ,                                         # U+fb51 ARABIC LETTER ALEF WASLA FINAL FORM
        'ﺁ' : r'(?:آ|آ|ﺁ|ﺂ)' ,                                            # U+fe81 ARABIC LETTER ALEF WITH MADDA ABOVE ISOLATED FORM
        'ﺂ' : r'(?:آ|آ|ﺁ|ﺂ)' ,                                            # U+fe82 ARABIC LETTER ALEF WITH MADDA ABOVE FINAL FORM
        'ﺃ' : r'(?:أ|أ?|ﺃ|ﺄ)' ,                                           # U+fe83 ARABIC LETTER ALEF WITH HAMZA ABOVE ISOLATED FORM
        'ﺄ' : r'(?:أ|أ?|ﺃ|ﺄ)' ,                                           # U+fe84 ARABIC LETTER ALEF WITH HAMZA ABOVE FINAL FORM
        'ﺇ' : r'(?:إ|إ?|ﺇ|ﺈ)' ,                                           # U+fe87 ARABIC LETTER ALEF WITH HAMZA BELOW ISOLATED FORM
        'ﺈ' : r'(?:إ|إ?|ﺇ|ﺈ)' ,                                           # U+fe88 ARABIC LETTER ALEF WITH HAMZA BELOW FINAL FORM
        'ٵ' : r'(?:ٔا|ٵ)' ,                                                # U+0675 ARABIC LETTER HIGH HAMZA ALEF ; erroneous char
        'ﺅ' : r'(?:ؤ|ؤ|ﺅ|ﺆ)' ,                                            # U+fe85 ARABIC LETTER WAW WITH HAMZA ABOVE ISOLATED FORM
        'ﺆ' : r'(?:ؤ|ؤ|ﺅ|ﺆ)' ,                                            # U+fe86 ARABIC LETTER WAW WITH HAMZA ABOVE FINAL FORM
        'ﺩ' : r'(?:د|ﺩ|ﺪ)' ,                                               # U+fea9 ARABIC LETTER DAL ISOLATED FORM
        'ﺪ' : r'(?:د|ﺩ|ﺪ)' ,                                               # U+feaa ARABIC LETTER DAL FINAL FORM
        'ﺫ' : r'(?:ذ|ﺫ|ﺬ)' ,                                               # U+feab ARABIC LETTER THAL ISOLATED FORM
        'ﺬ' : r'(?:ذ|ﺫ|ﺬ)' ,                                               # U+feac ARABIC LETTER THAL FINAL FORM
        'ﺭ' : r'(?:ر|ﺭ|ﺮ)' ,                                               # U+fead ARABIC LETTER REH ISOLATED FORM
        'ﺮ' : r'(?:ر|ﺭ|ﺮ)' ,                                               # U+feae ARABIC LETTER REH FINAL FORM
        'ﺯ' : r'(?:ز|ﺯ|ﺰ)' ,                                               # U+feaf ARABIC LETTER ZAIN ISOLATED FORM
        'ﺰ' : r'(?:ز|ﺯ|ﺰ)' ,                                               # U+feb0 ARABIC LETTER ZAIN FINAL FORM
        'ﮊ' : r'(?:ژ|ﮊ|ﮋ)' ,                                               # U+fb8a ARABIC LETTER JEH ISOLATED FORM
        'ﮋ' : r'(?:ژ|ﮊ|ﮋ)' ,                                               # U+fb8b ARABIC LETTER JEH FINAL FORM
        'ﻭ' : r'(?:و|ﻭ|ﻮ|ۥ)' ,                                              # U+feed ARABIC LETTER WAW ISOLATED FORM
        'ﻮ' : r'(?:و|ﻭ|ﻮ|ۥ)' ,                                              # U+feee ARABIC LETTER WAW FINAL FORM
        'ۥ' : r'(?:و|ﻭ|ﻮ|ۥ)' ,                                              # U+06e5 ARABIC SMALL WAW

        'ۀ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)ٔ|ۀ|ﮤ|ﮥ|ۂ)' ,                      # U+06c0 ARABIC LETTER HEH WITH YEH ABOVE
        'ﮤ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)ٔ|ۀ|ﮤ|ﮥ|ۂ)' ,                      # U+fba4 ARABIC LETTER HEH WITH YEH ABOVE ISOLATED FORM
        'ﮥ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)ٔ|ۀ|ﮤ|ﮥ|ۂ)' ,                      # U+fba5 ARABIC LETTER HEH WITH YEH ABOVE FINAL FORM
        'ۂ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)ٔ|ۀ|ﮤ|ﮥ|ۂ)' ,                      # U+06c2 ARABIC LETTER HEH GOAL WITH HAMZA ABOVE

        'ﺓ' : r'(?:ة|ﺓ|ﺔ|ۃ)' ,                                             # U+fe93 ARABIC LETTER TEH MARBUTA ISOLATED FORM
        'ﺔ' : r'(?:ة|ﺓ|ﺔ|ۃ)' ,                                             # U+fe94 ARABIC LETTER TEH MARBUTA FINAL FORM
        'ۃ' : r'(?:ة|ﺓ|ﺔ|ۃ)' ,                                             # U+06c3 ARABIC LETTER TEH MARBUTA GOAL
    })

    RASM_TRANSLIT = {
        'A' : 'ا' ,
        'B' : 'ٮ' ,
        'G' : 'ح' ,
        'D' : 'د' ,
        'R' : 'ر' ,
        'S' : 'س' ,
        'C' : 'ص' ,
        'T' : 'ط' ,
        'E' : 'ع' ,
        'F' : 'ڡ' ,
        'Q' : 'ٯ' ,
        'K' : 'ک' ,
        'L' : 'ل' ,
        'M' : 'م' ,
        'N' : 'ں' ,
        'H' : 'ه' ,
        'W' : 'و' ,
        'Y' : 'ی' ,
    }

    RASM_LETTERS = RASM_TRANSLIT.values()

    RASM_NYQ_MAPPING = {
        'ں' : 'ں' ,  # N
        'ن' : 'ں' ,
        'ڻ' : 'ں' ,
        'ڼ' : 'ں' ,
        'ڹ' : 'ں' ,
        'ݧ' : 'ں' ,
        'ݨ' : 'ں' ,
        'ݩ' : 'ں' ,
        'ڽ' : 'ں' ,
        'ﻥ' : 'ں' ,
        'ﻦ' : 'ں' ,
        'ی' : 'ی' ,  # Y
        'ي' : 'ی' ,
        'ى' : 'ی' ,
        'ۍ' : 'ی' ,
        'ݷ' : 'ی' ,
        'ې' : 'ی' ,
        'ۑ' : 'ی' ,
        'ؠ' : 'ی' ,
        'ئ' : 'ی' ,
        'ؽ' : 'ی' ,
        'ێ' : 'ی' ,
        'ݵ' : 'ی' ,
        'ݶ' : 'ی' ,
        'ࢨ' : 'ی' ,
        'ࢩ' : 'ی' ,
        'ؾ' : 'ی' ,
        'ؿ' : 'ی' ,
        'ے' : 'ی' ,
        'ۓ' : 'ی' ,
        'ݺ' : 'ی' ,
        'ݻ' : 'ی' ,
        'ﻯ' : 'ی' ,
        'ﻰ' : 'ی' ,
        'ﮮ' : 'ی' ,
        'ﮯ' : 'ی' ,
        'ی' : 'ی' ,
        'ﯼ' : 'ی' ,
        'ﯽ' : 'ی' ,
        'ﻲ' : 'ی' ,
        'ﮰ' : 'ی' ,
        'ﮱ' : 'ی' ,
        'ﺉ' : 'ی' ,
        'ﺊ' : 'ی' ,
        'ٯ' : 'ٯ' ,  # Q
        'ق' : 'ٯ' , 
        'ٯ' : 'ٯ' ,
        'ࢥ' : 'ٯ' ,
        'ڧ' : 'ٯ' ,
        'ڨ' : 'ٯ' ,
        'ﻕ' : 'ٯ' ,
        'ﻖ' : 'ٯ' ,
    }

    RASM_MAPPING = {
        'ب' : 'ٮ' ,  # B
        'ت' : 'ٮ' ,
        'ث' : 'ٮ' ,
        'پ' : 'ٮ' ,
        'ࢬ' : 'ٮ' ,
        'ݕ' : 'ٮ' ,
        'ࢠ' : 'ٮ' ,
        'ٻ' : 'ٮ' ,
        'ݐ' : 'ٮ' ,
        'ڀ' : 'ٮ' ,
        'ݒ' : 'ٮ' ,
        'ٹ' : 'ٮ' ,
        'ݖ' : 'ٮ' ,
        'ݔ' : 'ٮ' ,
        'ٺ' : 'ٮ' ,
        'ټ' : 'ٮ' ,
        'ݓ' : 'ٮ' ,
        'ٽ' : 'ٮ' ,
        'ٿ' : 'ٮ' ,
        'ݑ' : 'ٮ' ,
        'ﻧ' : 'ٮ' ,
        'ﻨ' : 'ٮ' ,
        'ﯾ' : 'ٮ' ,
        'ﯿ' : 'ٮ' ,
        'ﻳ' : 'ٮ' ,
        'ﻴ' : 'ٮ' ,
        'ۦ' : 'ٮ' ,
        'ﺋ' : 'ٮ' ,
        'ﺌ' : 'ٮ' ,
        'ﺏ' : 'ٮ' ,
        'ﺐ' : 'ٮ' ,
        'ﺑ' : 'ٮ' ,
        'ﺒ' : 'ٮ' ,
        'ﭖ' : 'ٮ' ,
        'ﭗ' : 'ٮ' ,
        'ﭘ' : 'ٮ' ,
        'ﭙ' : 'ٮ' ,
        'ﺕ' : 'ٮ' ,
        'ﺖ' : 'ٮ' ,
        'ﺗ' : 'ٮ' ,
        'ﺘ' : 'ٮ' ,
        'ﺙ' : 'ٮ' ,
        'ﺚ' : 'ٮ' ,
        'ﺛ' : 'ٮ' ,
        'ﺜ' : 'ٮ' ,
        'ن' : 'ٮ' ,
        'ں' : 'ٮ' ,
        'ڻ' : 'ٮ' ,
        'ڼ' : 'ٮ' ,
        'ڹ' : 'ٮ' ,
        'ݧ' : 'ٮ' ,
        'ݨ' : 'ٮ' ,
        'ݩ' : 'ٮ' ,
        'ڽ' : 'ٮ' ,
        'ﻥ' : 'ٮ' ,
        'ﻦ' : 'ٮ' ,
        'ي' : 'ٮ' ,
        'ى' : 'ٮ' ,
        'ی' : 'ٮ' ,
        'ۍ' : 'ٮ' ,
        'ݷ' : 'ٮ' ,
        'ې' : 'ٮ' ,
        'ۑ' : 'ٮ' ,
        'ؠ' : 'ٮ' ,
        'ئ' : 'ٮ' ,
        'ؽ' : 'ٮ' ,
        'ێ' : 'ٮ' ,
        'ݵ' : 'ٮ' ,
        'ݶ' : 'ٮ' ,
        'ࢨ' : 'ٮ' ,
        'ࢩ' : 'ٮ' ,
        'ؾ' : 'ٮ' ,
        'ؿ' : 'ٮ' ,
        'ے' : 'ٮ' ,
        'ۓ' : 'ٮ' ,
        'ݺ' : 'ٮ' ,
        'ݻ' : 'ٮ' ,
        'ﻯ' : 'ٮ' ,
        'ﻰ' : 'ٮ' ,
        'ﮮ' : 'ٮ' ,
        'ﮯ' : 'ٮ' ,
        'ی' : 'ٮ' ,
        'ﯼ' : 'ٮ' ,
        'ﯽ' : 'ٮ' ,
        'ﻲ' : 'ٮ' ,
        'ﮰ' : 'ٮ' ,
        'ﮱ' : 'ٮ' ,
        'ﺉ' : 'ٮ' ,
        'ﺊ' : 'ٮ' ,
        'ف' : 'ڡ' ,  # F
        'ﻑ' : 'ڡ' ,
        'ﻒ' : 'ڡ' ,
        'ڡ' : 'ڡ' ,
        'ڢ' : 'ڡ' ,
        'ݠ' : 'ڡ' ,
        'ڥ' : 'ڡ' ,
        'ݡ' : 'ڡ' ,
        'ڣ' : 'ڡ' ,
        'ڦ' : 'ڡ' ,
        'ࢤ' : 'ڡ' ,
        'ﻓ' : 'ڡ' ,
        'ﻔ' : 'ڡ' ,
        'ق' : 'ڡ' ,
        'ڤ' : 'ڡ' ,
        'ﻗ' : 'ڡ' ,
        'ﻘ' : 'ڡ' ,
        'ٯ' : 'ڡ' ,
        'ࢥ' : 'ڡ' ,
        'ڧ' : 'ڡ' ,
        'ڨ' : 'ڡ' ,
        'ﻕ' : 'ڡ' ,
        'ﻖ' : 'ڡ' ,
        'خ' : 'ح' ,  # G
        'ج' : 'ح' ,
        'چ' : 'ح' ,
        'ݮ' : 'ح' ,
        'ݼ' : 'ح' ,
        'ڃ' : 'ح' ,
        'ڄ' : 'ح' ,
        'چ' : 'ح' ,
        'ڇ' : 'ح' ,
        'ݘ' : 'ح' ,
        'ݯ' : 'ح' ,
        'ځ' : 'ح' ,
        'ݲ' : 'ح' ,
        'ڿ' : 'ح' ,
        'ڂ' : 'ح' ,
        'ݗ' : 'ح' ,
        'ࢢ' : 'ح' ,
        'څ' : 'ح' ,
        'ﺝ' : 'ح' ,
        'ﺞ' : 'ح' ,
        'ﺟ' : 'ح' ,
        'ﺠ' : 'ح' ,
        'ﺡ' : 'ح' ,
        'ﺢ' : 'ح' ,
        'ﺣ' : 'ح' ,
        'ﺤ' : 'ح' ,
        'ﺥ' : 'ح' ,
        'ﺦ' : 'ح' ,
        'ﺧ' : 'ح' ,
        'ﺨ' : 'ح' ,
        'ﭺ' : 'ح' ,
        'ﭻ' : 'ح' ,
        'ﭼ' : 'ح' ,
        'ﭽ' : 'ح' ,
        'ݪ' : 'ل' ,  # L
        'ࢦ' : 'ل' ,
        'ڸ' : 'ل' ,
        'ڵ' : 'ل' ,
        'ڶ' : 'ل' ,
        'ڷ' : 'ل' ,
        'ﻝ' : 'ل' ,
        'ﻞ' : 'ل' ,
        'ﻟ' : 'ل' ,
        'ﻠ' : 'ل' ,
        'ش' : 'س' ,  # S
        'ڛ' : 'س' ,
        'ݽ' : 'س' ,
        'ݾ' : 'س' ,
        'ښ' : 'س' ,
        'ݭ' : 'س' ,
        'ݜ' : 'س' ,
        'ݰ' : 'س' ,
        'ۺ' : 'س' ,
        'ڜ' : 'س' ,
        'ﺱ' : 'س' ,
        'ﺲ' : 'س' ,
        'ﺳ' : 'س' ,
        'ﺴ' : 'س' ,
        'ﺵ' : 'س' ,
        'ﺶ' : 'س' ,
        'ﺷ' : 'س' ,
        'ﺸ' : 'س' ,
        'ݦ' : 'م' ,  # M
        'ݥ' : 'م' , 
        'ࢧ' : 'م' , 
        'ﻡ' : 'م' , 
        'ﻢ' : 'م' , 
        'ﻣ' : 'م' , 
        'ﻤ' : 'م' , 
        'غ' : 'ع' ,  # E
        'ۼ' : 'ع' ,
        'ݝ' : 'ع' ,
        'ݟ' : 'ع' ,
        'ڠ' : 'ع' ,
        'ݞ' : 'ع' ,
        'ﻉ' : 'ع' ,
        'ﻊ' : 'ع' ,
        'ﻋ' : 'ع' ,
        'ﻌ' : 'ع' ,
        'ﻍ' : 'ع' ,
        'ﻎ' : 'ع' ,
        'ﻏ' : 'ع' ,
        'ﻐ' : 'ع' ,
        'ك' : 'ک' ,  # K
        'ڪ' : 'ک' ,
        'ګ' : 'ک' ,
        'گ' : 'ک' ,
        'ڰ' : 'ک' ,
        'ڲ' : 'ک' ,
        'ڳ' : 'ک' ,
        'ؼ' : 'ک' ,
        'ڮ' : 'ک' ,
        'ݤ' : 'ک' ,
        'ڮ' : 'ک' ,
        'ݢ' : 'ک' ,
        'ػ' : 'ک' ,
        'ڱ' : 'ک' ,
        'ݿ' : 'ک' ,
        'ڭ' : 'ک' ,
        'ڴ' : 'ک' ,
        'ݣ' : 'ک' ,
        'ﻙ' : 'ک' ,
        'ﻚ' : 'ک' ,
        'ﻛ' : 'ک' ,
        'ﻜ' : 'ک' ,
        'ﮎ' : 'ک' ,
        'ﮏ' : 'ک' ,
        'ﮐ' : 'ک' ,
        'ﮑ' : 'ک' ,
        'ﮒ' : 'ک' ,
        'ﮓ' : 'ک' ,
        'ﮔ' : 'ک' ,
        'ﮕ' : 'ک' ,
        'ض' : 'ص' ,  # C
        'ڝ' : 'ص' ,
        'ۻ' : 'ص' ,
        'ڞ' : 'ص' ,
        'ﺹ' : 'ص' ,
        'ﺺ' : 'ص' ,
        'ﺻ' : 'ص' ,
        'ﺼ' : 'ص' ,
        'ﺽ' : 'ص' ,
        'ﺾ' : 'ص' ,
        'ﺿ' : 'ص' ,
        'ﻀ' : 'ص' ,
        'ظ' : 'ط' ,  # T
        'ࢣ' : 'ط' ,
        'ڟ' : 'ط' ,
        'ﻁ' : 'ط' ,
        'ﻂ' : 'ط' ,
        'ﻃ' : 'ط' ,
        'ﻄ' : 'ط' ,
        'ﻅ' : 'ط' ,
        'ﻆ' : 'ط' ,
        'ﻇ' : 'ط' ,
        'ﻈ' : 'ط' ,
        'ھ' : 'ه' ,  # H
        'ة' : 'ه' ,
        'ہ' : 'ه' ,
        'ە' : 'ه' ,
        'ۀ' : 'ه' ,
        'ۂ' : 'ه' ,
        'ۿ' : 'ه' ,
        'ۃ' : 'ه' ,
        'ﮤ' : 'ه' ,
        'ﮥ' : 'ه' ,
        'ﺓ' : 'ه' ,
        'ﺔ' : 'ه' ,
        'ﮦ' : 'ه' ,
        'ﮧ' : 'ه' ,
        'ﮨ' : 'ه' ,
        'ﮩ' : 'ه' ,
        'ﻪ' : 'ه' ,
        'ﻫ' : 'ه' ,
        'ﻬ' : 'ه' ,
        'ﮪ' : 'ه' ,
        'ﮫ' : 'ه' ,
        'ﮬ' : 'ه' ,
        'ﮭ' : 'ه' ,
        'ٱ' : 'ا' ,  # A
        'أ' : 'ا' ,
        'إ' : 'ا' ,
        'آ' : 'ا' ,
        'ا' : 'ا' ,
        'ٳ' : 'ا' ,
        'ٲ' : 'ا' ,
        'ݳ' : 'ا' ,
        'ݴ' : 'ا' ,
        'ٵ' : 'ا' ,
        'ﺃ' : 'ا' ,
        'ﺄ' : 'ا' ,
        'ﺇ' : 'ا' ,
        'ﺈ' : 'ا' ,
        'ﺁ' : 'ا' ,
        'ﺂ' : 'ا' ,
        'ﺍ' : 'ا' ,
        'ﺎ' : 'ا' ,
        'ﭐ' : 'ا' ,
        'ﭑ' : 'ا' ,
        'ﴼ' : 'ا' ,
        'ۄ' : 'و' ,  # W
        'ۅ' : 'و' , 
        'ࢫ' : 'و' , 
        'ؤ' : 'و' , 
        'ۆ' : 'و' , 
        'ۇ' : 'و' , 
        'ۈ' : 'و' , 
        'ۉ' : 'و' , 
        'ۏ' : 'و' , 
        'ݸ' : 'و' , 
        'ݹ' : 'و' , 
        'ۊ' : 'و' , 
        'ۋ' : 'و' , 
        'ﻭ' : 'و' , 
        'ﻮ' : 'و' , 
        'ﺅ' : 'و' , 
        'ﺆ' : 'و' , 
        'ز' : 'ر' ,  # R
        'ړ' : 'ر' ,
        'ݛ' : 'ر' ,
        'ࢪ' : 'ر' ,
        'ڔ' : 'ر' ,
        'ڕ' : 'ر' ,
        'ڑ' : 'ر' ,
        'ڒ' : 'ر' ,
        'ۯ' : 'ر' ,
        'ݬ' : 'ر' ,
        'ږ' : 'ر' ,
        'ڗ' : 'ر' ,
        'ݫ' : 'ر' ,
        'ژ' : 'ر' ,
        'ڙ' : 'ر' ,
        'ݱ' : 'ر' ,
        'ﺭ' : 'ر' ,
        'ﺮ' : 'ر' ,
        'ﺯ' : 'ر' ,
        'ﺰ' : 'ر' ,
        'ﮊ' : 'ر' ,
        'ﮋ' : 'ر' ,
        'ذ' : 'د' ,  # D
        'ڈ' : 'د' ,
        'ډ' : 'د' ,
        'ڊ' : 'د' ,
        'ݚ' : 'د' ,
        'ڍ' : 'د' ,
        'ڈ' : 'د' ,
        'ۮ' : 'د' ,
        'ڋ' : 'د' ,
        'ݙ' : 'د' ,
        'ڌ' : 'د' ,
        'ڎ' : 'د' ,
        'ڏ' : 'د' ,
        'ڐ' : 'د' ,
        'ﺩ' : 'د' ,
        'ﺪ' : 'د' ,
        'ﺫ' : 'د' ,
        'ﺬ' : 'د' ,
    }

    ALIF = None  # defined only if flag ignore_alif is True

    CLUSTERS = { 'ﷲ' : [(TAG.BASE_DIS, 'ٱ'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ه')],

                'ﷰ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ے')],

                'ﷱ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ے')],

                'ﷳ' : [(TAG.BASE_DIS, 'ا'),
                        (TAG.BASE_CON, 'ك'),
                        (TAG.BASE_CON, 'ب'),
                        (TAG.BASE_DIS, 'ر')],

                'ﷴ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     'ُ'),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     'َ'),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     'َّ'),
                         (TAG.BASE_DIS, 'د')],

                'ﷵ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.BASE_CON, 'ع'),
                         (TAG.BASE_CON, 'م')],

                'ﷶ' : [(TAG.BASE_DIS, 'ر'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'و'),
                         (TAG.BASE_CON, 'ل')],

                'ﷷ' : [(TAG.BASE_CON, 'ع'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ه')],

                'ﷸ' : [(TAG.BASE_DIS, 'و'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﷹ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﷺ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َّ'),
                        (TAG.BASE_DIS, 'ى'),
                        (TAG.DIAC,     'ٰ'),
                        (TAG.BOUN,     ' '),
                        (TAG.BASE_DIS, 'ا'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َّٰ'),
                        (TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     'ُ'),
                        (TAG.BOUN,     ' '),
                        (TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     'ْ'),
                        (TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     'ِ'),
                        (TAG.BOUN,     ' '),
                        (TAG.BASE_DIS, 'و'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َّ'),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     'َ')],

                'ﷻ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َّ'),
                        (TAG.BOUN,     ' '),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_DIS, 'ا'),
                        (TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     'ُ'),
                        (TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     'ُ')],

                '﷼' : [(TAG.BASE_DIS, 'ر'),
                        (TAG.DIAC,     'ِ'),
                        (TAG.BASE_CON, 'ی'),
                        (TAG.DIAC,     'َ'),
                        (TAG.BASE_DIS, 'ا'),
                        (TAG.BASE_CON, 'ل')],

                '﷽' : [(TAG.BASE_CON, 'ب'),
                         (TAG.DIAC,     'ِ'),
                         (TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     'ْ'),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     'ِ'),
                         (TAG.BOUN,     ' '),
                         (TAG.BASE_DIS, 'ا'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.DIAC,     'َّٰ'),
                         (TAG.BASE_CON, 'ه'),
                         (TAG.DIAC,     'ِ'),
                         (TAG.BOUN,     ' '),
                         (TAG.BASE_DIS, 'ا'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.BASE_DIS, 'ر'),
                         (TAG.DIAC,     'َّ'),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     'ْ'),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     'َٰ'),
                         (TAG.BASE_CON, 'ن'),
                         (TAG.DIAC,     'ِ'),
                         (TAG.BOUN,     ' '),
                         (TAG.BASE_DIS, 'ا'),
                         (TAG.BASE_CON, 'ل'),
                         (TAG.BASE_DIS, 'ر'),
                         (TAG.DIAC,     'َّ'),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     'ِ'),
                         (TAG.BASE_CON, 'ي'),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     'ِ' )],

                'ﯗ' : [(TAG.BASE_DIS, 'و'),
                       (TAG.DIAC,     'ُ' )],

                'ﯘ' : [(TAG.BASE_DIS, 'و'),
                       (TAG.DIAC,     'ُ' )],

                'ﯝ' : [(TAG.BASE_DIS, 'ؤ'),
                       (TAG.DIAC,     'ُ')],

                'ﯪ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ا')],

                'ﯫ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ا')],

                'ﯬ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ە')],

                'ﯭ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ە')],

                'ﯮ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'و')],

                'ﯯ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'و')],

                'ﯰ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۇ')],

                'ﯱ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۇ')],

                'ﯲ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۆ')],

                'ﯳ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۆ')],

                'ﯴ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۈ')],

                'ﯵ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ۈ')],

                'ﯶ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ې')],

                'ﯷ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ې')],

                'ﯸ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ې')],

                'ﯹ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_DIS,  'ى')],

                'ﯺ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_DIS,  'ى')],

                'ﯻ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_DIS,  'ى')],

                'ﰀ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ج')],

                'ﰁ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ح')],

                'ﰂ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰃ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰄ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰅ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰆ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰇ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﰈ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON,  'م')],

                'ﰉ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰊ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰋ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰌ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰍ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﰎ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰏ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰐ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰑ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰒ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰓ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰔ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰕ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰖ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰗ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰘ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰙ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰚ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰛ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰜ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰝ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰞ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﰟ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰠ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰡ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰢ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰣ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰤ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'خ')],

                'ﰥ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'م')],

                'ﰦ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'ح')],

                'ﰧ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'م')],

                'ﰨ' : [(TAG.BASE_CON, 'ظ'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'م')],

                'ﰩ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'ج')],

                'ﰪ' : [(TAG.BASE_CON,  'ع'),
                        (TAG.DIAC,      '' ),
                        (TAG.BASE_CON,  'م')],

                'ﰫ' : [(TAG.BASE_CON, 'غ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ج')],

                'ﰬ' : [(TAG.BASE_CON, 'غ'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'م')],

                'ﰭ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ج')],

                'ﰮ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ح' )],

                'ﰯ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'خ')],

                'ﰰ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,   'م')],

                'ﰱ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_DIS,   'ى')],

                'ﰲ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ي')],

                'ﰳ' : [(TAG.BASE_CON, 'ق'),
                       (TAG.DIAC,      '' ),
                       (TAG.BASE_CON,  'ح')],

                'ﰴ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰵ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰶ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰷ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ا')],

                'ﰸ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﰹ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﰺ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﰻ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ل')],

                'ﰼ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﰽ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﰾ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﰿ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﱀ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﱁ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﱂ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﱃ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱄ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱅ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﱆ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﱇ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﱈ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﱉ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱊ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱋ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﱌ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﱍ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﱎ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﱏ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱐ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱑ' : [(TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﱒ' : [(TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﱓ' : [(TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱔ' : [(TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱕ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﱖ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﱗ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﱘ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﱙ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱚ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱛ' : [(TAG.BASE_DIS, 'ذ'),
                       (TAG.DIAC,     'ٰ')],

                'ﱜ' : [(TAG.BASE_DIS, 'ر'),
                       (TAG.BASE_CON, 'ٰ')],

                'ﱝ' : [(TAG.BASE_DIS, 'ى'),
                       (TAG.DIAC,     'ٰ')],

                'ﱞ' : [(TAG.DIAC, 'ٌّ')],

                'ﱟ' : [(TAG.DIAC, 'ٍّ')],

                'ﱠ' : [(TAG.DIAC, 'َّ')],

                'ﱡ' : [(TAG.DIAC, 'ُّ')],

                'ﱢ' : [(TAG.DIAC, 'ِّ')],

                'ﱣ' : [(TAG.DIAC, 'ّٰ')],

                'ﱤ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ر')],

                'ﱥ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ز')],

                'ﱦ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﱧ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﱨ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱩ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﱪ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ر')],

                'ﱫ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ز')],

                'ﱬ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﱭ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﱮ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﱯ' : [(TAG.BASE_CON, 'ب'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﱰ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ر')],

                'ﱱ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ز')],

                'ﱲ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﱳ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﱴ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﱵ' : [(TAG.BASE_CON, 'ت'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﱶ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ر')],

                'ﱷ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ز')],

                'ﱸ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﱹ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﱺ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﱻ' : [(TAG.BASE_CON, 'ث'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﱼ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﱽ' : [(TAG.BASE_CON, 'ف'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﱾ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﱿ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﲀ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ا')],

                'ﲁ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ل')],

                'ﲂ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲃ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﲄ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﲅ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲆ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﲇ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﲈ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ا')],

                'ﲉ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲊ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ر')],

                'ﲋ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ز')],

                'ﲌ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲍ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﲎ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﲏ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﲐ' : [(TAG.BASE_DIS, 'ى'),
                       (TAG.DIAC,     'ٰ')],

                'ﲑ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ر')],

                'ﲒ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ز')],

                'ﲓ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﲔ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ن')],

                'ﲕ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﲖ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﲗ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﲘ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ح')],

                'ﲙ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'خ')],

                'ﲚ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﲛ' : [(TAG.BASE_CON, 'ئ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ه')],

                'ﲜ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲝ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲞ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﲟ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲠ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﲡ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲢ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲣ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﲤ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲥ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﲦ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲧ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲨ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲩ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲪ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲫ' : [(TAG.BASE_CON, 'خ'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﲬ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲭ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲮ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲯ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﲰ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲱ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲲ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﲳ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲴ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲵ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲶ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﲷ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲸ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﲹ' : [(TAG.BASE_CON, 'ظ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲺ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲻ' : [(TAG.BASE_CON, 'ع'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﲼ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲽ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﲾ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﲿ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﳀ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﳁ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳂ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﳃ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳄ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﳅ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﳆ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﳇ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ل')],

                'ﳈ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳉ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﳊ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﳋ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'خ')],

                'ﳌ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﳍ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ه')],

                'ﳎ' : [(TAG.BASE_CON, 'م'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﳏ' : [(TAG.BASE_CON, 'م'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ح')],

                'ﳐ' : [(TAG.BASE_CON, 'م'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'خ')],

                'ﳑ' : [(TAG.BASE_CON, 'م'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﳒ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﳓ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ح')],

                'ﳔ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'خ')],

                'ﳕ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﳖ' : [(TAG.BASE_CON, 'ن'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ه')],

                'ﳗ' : [(TAG.BASE_CON, 'ه'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﳘ' : [(TAG.BASE_CON, 'ه'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'م')],

                'ﳙ' : [(TAG.BASE_CON, 'ه'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ٰ')],

                'ﳚ' : [(TAG.BASE_CON, 'ي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ج')],

                'ﳛ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﳜ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﳝ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳞ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳟ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳠ' : [(TAG.BASE_CON, 'ئ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳡ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳢ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳣ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳤ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳥ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳦ' : [(TAG.BASE_CON, 'ث'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳧ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳨ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳩ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳪ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳫ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ل')],

                'ﳬ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳭ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳮ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳯ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﳰ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﳱ' : [(TAG.BASE_CON, 'يه'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ه')],

                'ﳲ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'َّ')],

                'ﳳ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ُّ')],

                'ﳴ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ِّ')],

                'ﳵ' : [(TAG.BASE_CON, 'طى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﳶ' : [(TAG.BASE_CON, 'طي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﳷ' : [(TAG.BASE_CON, 'عى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﳸ' : [(TAG.BASE_CON, 'عي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﳹ' : [(TAG.BASE_CON, 'غى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﳺ' : [(TAG.BASE_CON, 'غي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﳻ' : [(TAG.BASE_CON, 'سى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﳼ' : [(TAG.BASE_CON, 'سي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﳽ' : [(TAG.BASE_CON, 'شى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﳾ' : [(TAG.BASE_CON, 'شي'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_CON, 'ي')],

                'ﳿ' : [(TAG.BASE_CON, 'حى'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ى')],

                'ﴀ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴁ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﴂ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴃ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﴄ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴅ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﴆ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴇ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﴈ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴉ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﴊ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﴋ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﴌ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴍ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴎ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴏ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴐ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴑ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴒ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴓ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴔ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴕ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴖ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴗ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴘ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴙ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴚ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴛ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴜ' : [(TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴝ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴞ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴟ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴠ' : [(TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴡ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴢ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴣ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﴤ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﴥ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﴦ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﴧ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﴨ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴩ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴪ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴫ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴬ' : [(TAG.BASE_CON, 'ض'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ر')],

                'ﴭ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﴮ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﴯ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﴰ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴱ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﴲ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ه')],

                'ﴳ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴴ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﴵ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﴶ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﴷ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﴸ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﴹ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﴺ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴻ' : [(TAG.BASE_CON, 'ظ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﴼ' : [(TAG.BASE_CON, 'ا'),
                       (TAG.DIAC,     'ً')],

                'ﴽ' : [(TAG.BASE_DIS, 'ا'),
                      (TAG.DIAC,     'ً')],

                'ﵐ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵑ' : [(TAG.BASE_CON, 'ت'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﵒ' : [(TAG.BASE_CON, 'ت'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﵓ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵔ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵕ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﵖ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﵗ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﵘ' : [(TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵙ' : [(TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵚ' : [(TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﵛ' : [(TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﵜ' : [(TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﵝ' : [(TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵞ' : [(TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﵟ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﵠ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﵡ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﵢ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵣ' : [(TAG.BASE_CON, 'س'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵤ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵥ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵦ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵧ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵨ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵩ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﵪ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ')],

                'ﵫ' : [(TAG.BASE_CON, 'ش'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ')],

                'ﵬ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵭ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵮ' : [(TAG.BASE_CON, 'ض'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﵯ' : [(TAG.BASE_CON, 'ض'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵰ' : [(TAG.BASE_CON, 'ض'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵱ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﵲ' : [(TAG.BASE_CON, 'ط'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﵳ' : [(TAG.BASE_CON, 'ط'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵴ' : [(TAG.BASE_CON, 'ط'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﵵ' : [(TAG.BASE_CON, 'ع'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﵶ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵷ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵸ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﵹ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵺ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﵻ' : [(TAG.BASE_CON, 'غ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﵼ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵽ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﵾ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﵿ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶀ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶁ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶂ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﶃ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﶄ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج')],

                'ﶅ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶆ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶇ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﶈ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﶉ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﶊ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﶋ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶌ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح')],

                'ﶍ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﶎ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﶏ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م')],

                'ﶒ' : [(TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ')],

                'ﶓ' : [(TAG.BASE_CON, 'ه'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج')],

                'ﶔ' : [(TAG.BASE_CON, 'ه'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶕ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶖ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_DIS, 'ى')],

                'ﶗ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶘ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶙ' : [(TAG.BASE_CON, 'ن'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﶚ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶛ' : [(TAG.BASE_CON, 'ن'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_DIS, 'ى')],

                'ﶜ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶝ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶞ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶟ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶠ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﶡ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶢ' : [(TAG.BASE_CON, 'ت'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ى')],

                'ﶣ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶤ' : [(TAG.BASE_CON, 'ت'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ى')],

                'ﶥ' : [(TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶦ' : [(TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ى')],

                'ﶧ' : [(TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'م'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ى')],

                'ﶨ' : [(TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ى')],

                'ﶩ' : [(TAG.BASE_CON, 'ص'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶪ' : [(TAG.BASE_CON, 'ش'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶫ' : [(TAG.BASE_CON, 'ض'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶬ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶭ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶮ' : [(TAG.BASE_CON, 'ي'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶯ' : [(TAG.BASE_CON, 'ي'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶰ' : [(TAG.BASE_CON, 'ي'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶱ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶲ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶳ' : [(TAG.BASE_CON, 'ن'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﶴ' : [(TAG.BASE_CON, 'ق'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﶵ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶶ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶷ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶸ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﶹ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'خ'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶺ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶻ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶼ' : [(TAG.BASE_CON, 'ل'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﶽ' : [(TAG.BASE_CON, 'ن'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح')],

                'ﶾ' : [(TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﶿ' : [(TAG.BASE_CON, 'ح'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﷀ' : [(TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﷁ' : [(TAG.BASE_CON, 'ف'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﷂ' : [(TAG.BASE_CON, 'ب'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ح'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ي')],

                'ﷃ' : [(TAG.BASE_CON, 'ك'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﷄ' : [(TAG.BASE_CON, 'ع'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'ج'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﷅ' : [(TAG.BASE_CON, 'ص'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م'),
                        (TAG.DIAC,     '' ),
                        (TAG.BASE_CON, 'م')],

                'ﷆ' : [(TAG.BASE_CON, 'س'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'خ'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﷇ' : [(TAG.BASE_CON, 'ن'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ج'),
                         (TAG.DIAC,     '' ),
                         (TAG.BASE_CON, 'ي')],

                'ﹱ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ً')],

                'ﹷ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'َ')],

                'ﹹ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ُ')],

                'ﹻ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ِ')],

                'ﹽ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ّ')],

                'ﹿ' : [(TAG.BOUN, 'ـ'),
                       (TAG.DIAC, 'ْ')],

                'ﻵ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'آ')],

                'ﻶ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'آ')],

                'ﻷ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'أ')],

                'ﻸ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'أ')],

                'ﻹ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'إ')],

                'ﻺ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'إ')],

                'ﻻ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ا')],

                'ﻼ' : [(TAG.BASE_CON, 'ل'),
                       (TAG.DIAC,     '' ),
                       (TAG.BASE_DIS, 'ا')]
    }

    def __init__(self, config={'rasm' : False ,
                               'diac_synonym' : True ,
                               'diac_expansion' : True ,
                               'ignore_diac' : False ,
                               'cluster_enable' : True ,
                               'spaces_wild' : True ,
                               'universal_ha' : True ,
                               'extended_ezafe' : True ,
                               'multishape_hamza' : True ,
                               'ignore_alif' : False ,
                               'regex' : False ,
                               'exact' : False
        }):
        """ Constructor for configuration object.

        Args:
            config(dict): configuration options.

        Instance attributes:
            rasm (bool): support archigraphemic search
            diac_synonym (bool): if True, match diacritics in the pattern with all their possible typographic equivalences.
            diac_expansion (bool): if True, base consonants that don't have diacritics indicated in pattern may have them in text.
            cluster_enable(bool): support the so called Unicode ligatures.
            spaces_wild (bool): if True, all letterblocks in pattern may be followed by optional spaces in text.
            universal_ha(bool): map all ha glyphs, including ta marbuta.
            multishape_hamza (bool): if True, isolated hamza won't match with hamzas of other shapes.
            ignore_alif (bool): if True, ignore all alifs for searching.
            regex (bool): if True, support regular expressions.
            exact (bool): if True, apply exact search and ignore the other configuration options.

            base_con_mapping (dict): inventory of replacements for base connected letters and clusters.
            base_dis_mapping (dict): inventory of replacements for base disconnected letters and clusters.

            clusters (dict): pairs of cluster code and list of segments it is equivalent to.
            rules (list): segmentation rules.

        """
        self.rasm:                bool = False
        self.diac_synonym:        bool = True
        self.diac_expansion:      bool = True
        self.ignore_diac:         bool = False
        self.cluster_enable:      bool = True
        self.spaces_wild:         bool = True
        self.universal_ha:        bool = True
        self.extended_ezafe:      bool = False
        self.multishape_hamza:    bool = False
        self.ignore_alif:         bool = False
        self.regex:               bool = False
        self.exact:               bool = False

        self.base_con_mapping = OrderedDict(SearchConfig._BASE_CON_MAPPING)
        self.base_dis_mapping = OrderedDict(SearchConfig._BASE_DIS_MAPPING)

        self.clusters = None
        self.rules = None

        # check if there are missing diacritics in DIAC
        if set(c for gr in (SearchConfig.DIAC_EXP_MAPPING.keys()|SearchConfig.DIAC_MAPPING.keys()) for c in gr)-set(SearchConfig.DIAC):
            print('Warning: not all diacritics in diacritic clusters are included in DIAC ', file=sys.stderr) #TRACE

        try:
            self.rasm = config['rasm']
            self.diac_synonym = config['diac_synonym']
            self.diac_expansion = config['diac_expansion']
            self.ignore_diac = config['ignore_diac']
            self.cluster_enable = config['cluster_enable']
            self.spaces_wild = config['spaces_wild']
            self.universal_ha = config['universal_ha']
            self.extended_ezafe = config['extended_ezafe']
            self.multishape_hamza = config['multishape_hamza']
            self.ignore_alif = config['ignore_alif']
            self.regex = config['regex']
            self.exact = config['exact']
            if self.rasm:
                self.spaces_wild = True
        except KeyError as err:
            print(f'Error: {err} is an invalid key for dict ‘nginx‘', file=sys.stderr) #FIXME
            sys.exit(1)

        if self.universal_ha:

            extra = { 'ه' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﻩ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﻪ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﻫ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﻬ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ھ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮪ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮫ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮬ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮭ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ہ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮦ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮧ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮨ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮩ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ے' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮮ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﮯ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  

            }

            self.base_con_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_con_mapping.move_to_end(pat, last=False)

            extra = { 'ة' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﺓ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ﺔ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
                      'ۃ' : r'(?:ه|ة|ﺓ|ﺔ|ۃ|ﻩ|ﻪ|ﻫ|ﻬ|ھ|ﮪ|ﮫ|ﮬ|ﮭ|ہ|ﮦ|ﮧ|ﮨ|ﮩ)' ,  
            }

            self.base_dis_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_dis_mapping.move_to_end(pat, last=False)


        if self.extended_ezafe:
            
            extra = { 'ۀ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,  # U+06c0 ARABIC LETTER HEH WITH YEH ABOVE
                      'ﮤ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,  # U+fba4 ARABIC LETTER HEH WITH YEH ABOVE ISOLATED FORM
                      'ﮥ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,  # U+fba5 ARABIC LETTER HEH WITH YEH ABOVE FINAL FORM
                      'ۂ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,  # U+06c2 ARABIC LETTER HEH GOAL WITH HAMZA ABOVE
            }

            self.base_dis_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_dis_mapping.move_to_end(pat, last=False)

            extra = { 'ه‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ‌ی'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ ی' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ‌ي'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ ي' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ‌ى'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ ى' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ه ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ھ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ہ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻩ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﻪ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮪ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮫ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮦ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ‌ٔ'  : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
                      'ﮧ ٔ' : r'(?:(?:ه|ھ|ہ|ﻩ|ﻪ|ﮪ|ﮫ|ﮦ|ﮧ)(?:‌| )(?:ی|ي|ى|ٔ)|ۀ|ﮤ|ﮥ|ۂ)' ,
            }

            self.base_con_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_con_mapping.move_to_end(pat, last=False)


        if self.multishape_hamza:

            extra = {
                'ئ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' , #FIXME AGM180820 added ىِٕ
                'ﺉ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺊ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺋ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺌ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ۓ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﮰ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﮱ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ىء'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ىٕ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' , #FIXME AGM180820
            }


            self.base_con_mapping.update(extra)
            
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_con_mapping.move_to_end(pat, last=False)

            extra = {
                'ء'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'أ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'إ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ؤ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'آ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ٱ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺅ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺆ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ؤ' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ىٔ' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ىء' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ا۬' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'أ' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'إ' : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺃ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺄ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺇ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺈ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺁ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﺂ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﭐ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ﭑ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ٕ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                'ٔ'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
                '۬'  : r'(?:ء|أ|إ|ئ|ؤ|آ|ٱ|ىء|ﺉ|ﺊ|ﺋ|ﺌ|ۓ|ﮰ|ﮱ|ىِٕ|ﺅ|ﺆ|ؤ|ىٔ|ا(?:۬|ٔ|ٕ|ٓ)|ﺃ|ﺄ|ﺇ|ﺈ|ﺁ|ﺂ|ﭐ|ﭑ|ٕ|ٔ|۬)' ,
            }

            self.base_dis_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_dis_mapping.move_to_end(pat, last=False)

        # Fragmina verborum
        #  Titivillus colligit horum
        #   Quibus die mille
        #    Vicibus se sarcinat ille

        # SWAPPED SHADDA

        if self.diac_synonym:

            if self.diac_expansion:
                extra = { 'ًا'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ًﺍ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ًﺎ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'اً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََا'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََﺍ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََﺎ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'اََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰا'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰﺍ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰﺎ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'اࣰ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍࣰ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎࣰ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'َۢا'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'َۢﺍ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'َۢﺎ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'اَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ){D}(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ){D}(ً|ََ|ࣰ|َۢ)))'.format(D=SearchConfig.DIAC_OPT) ,
                }

            else:
                extra = { 'ًا'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ًﺍ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ًﺎ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'اً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺍً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺎً'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ََا'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ََﺍ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ََﺎ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'اََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺍََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺎََ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ࣰا'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ࣰﺍ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ࣰﺎ'   : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'اَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺍَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))',
                          'ﺎَۢ'  : r'(?:(?:(ً|ََ|ࣰ|َۢ)(?:ا|ﺍ|ﺎ))|(?:(?:ا|ﺍ|ﺎ)(ً|ََ|ࣰ|َۢ)))'
                }

            self.base_dis_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat) > 1:
                    self.base_dis_mapping.move_to_end(pat, last=False)
        else:
            if self.diac_expansion:
                extra = { 'ًا'   : r'(?:ً{D}ا|ا{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ًﺍ'   : r'(?:ً{D}ﺍ|ﺍ{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ًﺎ'   : r'(?:ً{D}ﺎ|ﺎ{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'اً'   : r'(?:ً{D}ا|ا{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍً'   : r'(?:ً{D}ﺍ|ﺍ{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎً'   : r'(?:ً{D}ﺎ|ﺎ{D}ً)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََا'  : r'(?:ا{D}ََ|ََ{D}ا)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََﺍ'  : r'(?:ﺍ{D}ََ|ََ{D}ﺍ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ََﺎ'  : r'(?:ﺎ{D}ََ|ََ{D}ﺎ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'اََ'  : r'(?:ََ{D}ا|ا{D}ََ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍََ'  : r'(?:ََ{D}ﺍ|ﺍ{D}ََ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎََ'  : r'(?:ََ{D}ﺎ|ﺎ{D}ََ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰا'   : r'(?:ࣰ{D}ا|ا{D}ࣰ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰﺍ'   : r'(?:ࣰ{D}ﺍ|ﺍ{D}ࣰ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ࣰﺎ'   : r'(?:ࣰ{D}ﺎ|ﺎ{D}ࣰ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'اَۢ'  : r'(?:َۢ{D}ا|ا{D}َۢ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺍَۢ'  : r'(?:َۢ{D}ﺍ|ﺍ{D}َۢ)'.format(D=SearchConfig.DIAC_OPT) ,
                          'ﺎَۢ'  : r'(?:َۢ{D}ﺎ|ﺎ{D}َۢ)'.format(D=SearchConfig.DIAC_OPT) ,
                }
            else:
                extra = { 'ًا'   : r'(?:ًا|اً)' ,
                          'ًﺍ'   : r'(?:ًﺍ|ﺍً)' ,
                          'ًﺎ'   : r'(?:ًﺎ|ﺎً)' ,
                          'اً'   : r'(?:ًا|اً)' ,
                          'ﺍً'   : r'(?:ًﺍ|ﺍً)' ,
                          'ﺎً'   : r'(?:ًﺎ|ﺎً)' ,
                          'ََا'  : r'(?:ََا|اََ)' ,
                          'ََﺍ'  : r'(?:ََﺍ|ﺍََ)' ,
                          'ََﺎ'  : r'(?:ََﺎ|ﺎََ)' ,
                          'اََ'  : r'(?:ََا|اََ)' ,
                          'ﺍََ'  : r'(?:ََﺍ|ﺍََ)' ,
                          'ﺎََ'  : r'(?:ََﺎ|ﺎََ)' ,
                          'ࣰا'   : r'(?:اࣰ|ࣰا)' ,
                          'ࣰﺍ'   : r'(?:ﺍࣰ|ࣰﺍ)' ,
                          'ࣰﺎ'   : r'(?:ﺎࣰ|ࣰﺎ)' ,
                          'اَۢ'  : r'(?:َۢا|َۢا)' ,
                          'ﺍَۢ'  : r'(?:َۢﺍ|َۢﺍ)' ,
                          'ﺎَۢ'  : r'(?:َۢﺎ|َۢﺎ)' ,
                }

            self.base_dis_mapping.update(extra)
            for pat, rep in extra.items():
                if len(pat)>1:
                    self.base_dis_mapping.move_to_end(pat, last=False)

        # NOTE: ? + * . | (...) and escaping supported, always use parenthesis with quantifiers
        #       regardless if they refer only to the preceding character, eg. use a(b)? instead of ab?
        if self.regex:
            SearchConfig.REGEX_REGEX = re.compile(r'(?:\\[\(\|\)\?\+\*.]|[^\(\|\)\?\+\*.])+')

        if self.ignore_alif:
            SearchConfig.ALIF = set(k for k, v in SearchConfig.RASM_MAPPING.items() if v=='ا') | set('ٰ') # dagger

        
        #### UPDATE RULES ####

        self.rules = [(TAG.BASE_CON, r'(?:%s)'  % '|'.join(self.base_con_mapping)),
                      (TAG.BASE_DIS, r'(?:%s)'  % '|'.join(self.base_dis_mapping)),
                      (TAG.DIAC,     r'(?:%s)+' % '|'.join(SearchConfig.DIAC)),
                      (TAG.BOUN,     r'.')
        ]

        if self.cluster_enable:

            self.rules.insert(-2, (TAG.CLUS, r'(?:%s)'  % '|'.join(SearchConfig.CLUSTERS)))

            # clusters in pattern should't have explicit diacritics, but clusters in text should have them
            self.clusters_pat = {clus : [(Segment(tag, '') if tag==TAG.DIAC else Segment(tag, tok)) for tag, tok in seq]
                                          for clus, seq in SearchConfig.CLUSTERS.items()}
            self.clusters_text = {clus : [Segment(tag, tok) for tag, tok in seq] for clus, seq in SearchConfig.CLUSTERS.items()}


    def __str__(self):
        return f'''SearchConfig({self.rasm=}, {self.diac_synonym=}, {self.diac_expansion=}, {self.cluster_enable=}, {self.spaces_wild=},
        {self.universal_ha=}, {self.extended_ezafe=}, {self.multishape_hamza=}, {self.ignore_alif=}, {self.regex=}, {self.exact=})'''


