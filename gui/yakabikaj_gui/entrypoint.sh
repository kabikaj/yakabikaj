#!/bin/sh

cd ./django

python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput

gunicorn yakabikaj_gui.wsgi:application --bind 0.0.0.0:8001